<?php $page = commonhelper::getPage(1); 
$title = isset($page['page_title'])?$page['page_title']:'';
$meta_desc = isset($page['page_meta_desc'])?$page['page_meta_desc']:'';
$keyword = isset($page['page_meta_keyword'])?$page['page_meta_keyword']:'';
?>
<?php $__env->startSection('title'); ?>
   <?php echo e($title); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('description',$meta_desc); ?>
<?php $__env->startSection('keywords',$keyword); ?>

<?php $__env->startSection('header_styles'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
   <!-- start of hero -->   
        <section class="her hero-slider-wrapper">
            <div class="her-slider">
                <div class="slide">
                    <img src="<?php echo e(asset('front/images/slider/about.jpg')); ?>" alt class="slider-bg">
                    
                </div>

            </div>
        </section>
        <!-- end of hero slider -->
        <!-- start of services-serction-s3 -->
            <section class="services-serction-s3 section-padding">
            <div class="container">
            <div class="row section-title-s6">
            <?php echo isset($page['page_desc'])? html_entity_decode(htmlspecialchars_decode(($page['page_desc']))):''; ?>
             </div>
            </div> <!-- end container -->
            </section>
            <!-- end of services-serction-s3 -->
             <?php echo $__env->make('front.testimonial', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>;

<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer_script'); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts/frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>