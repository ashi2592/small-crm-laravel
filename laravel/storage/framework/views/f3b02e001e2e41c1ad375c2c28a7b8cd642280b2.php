
<!DOCTYPE html>
<html>
<head>
    <title>  <?php $__env->startSection('title'); ?>
            | Admin
        <?php echo $__env->yieldSection(); ?> </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="shortcut icon" href="<?php echo e(asset('assets/img/logo1.ico')); ?>"/>
    <!-- Global styles -->
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/css/components.css')); ?>" />
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/css/custom.css')); ?>" />
    <!--End of Global styles -->
    <!--Plugin styles-->
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/datepicker/css/bootstrap-datepicker.min.css')); ?>">
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/select2/css/select2.min.css')); ?>"/>
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')); ?>"/>
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/wow/css/animate.css')); ?>"/>
    <!--End of Plugin styles-->
    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/css/pages/login1.css')); ?>"/>
    <!--End of Page level styles-->
</head>
<body>
<div class="preloader" style=" position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  z-index: 100000;
  backface-visibility: hidden;
  background: #ffffff;">
    <div class="preloader_img" style="width: 200px;
  height: 200px;
  position: absolute;
  left: 48%;
  top: 48%;
  background-position: center;
z-index: 999999">
        <img src="<?php echo e(asset('assets/img/loader.gif')); ?>" style=" width: 40px;" alt="loading...">
    </div>
</div>
<?php if($errors): ?>
<span class="help-block">
<strong><?php print_r($errors->first()); ?></strong>
</span>
<?php endif; ?>

     <?php echo $__env->yieldContent('content'); ?>

<!-- global js -->
<script type="text/javascript" src="<?php echo e(asset('assets/js/jquery.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('assets/js/tether.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('assets/js/bootstrap.min.js')); ?>"></script>
<!-- end of global js-->
<!--Plugin js-->
<script type="text/javascript" src="<?php echo e(asset('assets/vendors/datepicker/js/bootstrap-datepicker.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('assets/vendors/select2/js/select2.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('assets/vendors/wow/js/wow.min.js')); ?>"></script>
<!--End of plugin js-->
<!--Page level js-->
<script type="text/javascript" src="<?php echo e(asset('assets/js/pages/register.js')); ?>"></script>
<!-- end of page level js -->
</body>

</html>