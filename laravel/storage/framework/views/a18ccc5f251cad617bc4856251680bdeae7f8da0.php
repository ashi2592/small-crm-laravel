<?php $__env->startSection('title'); ?>
    pricing
    ##parent-placeholder-3c6de1b7dd91465d437ef415f94f36afc1fbc8a8##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header_styles'); ?>
    <!--Plugin styles-->
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/select2/css/select2.min.css')); ?>"/>
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/css/pages/dataTables.bootstrap.css')); ?>"/>
    <!--End of plugin styles-->
    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/css/pages/tables.css')); ?>"/>
    <!-- end of page level styles -->

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <header class="head">
        <div class="main-bar">
            <div class="row no-gutters">
                <div class="col-lg-6 col-sm-4">
                    <h4 class="nav_top_align">
                        <i class="fa fa-spinner"></i>
                        pricing
                    </h4>
                </div>
                <div class="col-lg-6 col-sm-8 col-12">
                    <ol class="breadcrumb float-right  nav_breadcrumb_top_align">
                        <li class="breadcrumb-item">
                            <a href="index1">
                                <i class="fa fa-home" data-pack="default" data-tags=""></i> Dashboard
                            </a>
                        </li>
                        <li class="active breadcrumb-item">pricing</li>
                    </ol>
                </div>
            </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
            <div class="card">
                <div class="card-header bg-white">
                    pricing
                </div>
                <div class="card-block m-t-35" id="user_body">
                    <div class="table-toolbar">
                        <div class="btn-group">
                            <a href="addpricing" id="editable_table_new" class=" btn btn-default">
                                Add pricing <i class="fa fa-plus"></i>
                            </a>
                        </div>
                        <div class="btn-group float-right users_grid_tools">
                            <div class="tools"></div>
                        </div>
                    </div>
                    <div>
                        <div>
                            <table class="table  table-striped table-bordered table-hover dataTable no-footer"
                                   id="editable_table" role="grid">
                                <thead>
                                <tr role="row">
                                    <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">pricing Id</th>
                                    <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">pricing Name</th>
                                    <th>Action</th>
                                 </tr>
                                </thead>
                                <tbody>                                   
                                    <?php foreach ($result as $key => $value) :?>
                                <tr role="row" class="even">
                                    <td class="sorting_1"><?php echo e($value['id']); ?></td>
                                    <td><?php echo e($value['pricing']); ?></td>
                                   
                                    <td><a href="<?php echo e(URL::to('admin/addpricing')); ?>?id=<?php echo e($value['id']); ?>" data-toggle="tooltip" data-placement="top"
                                           title="View/Edit pricing"><i class="fa fa-eye text-success"></i></a>&nbsp; &nbsp;&nbsp;
                                        &nbsp;<a href="<?php echo e(URL::to('admin/deletepricing')); ?>?id=<?php echo e($value['id']); ?>" data-toggle="tooltip" data-placement="top"
                                           title="
                                           delete pricing"><i class="fa fa-trash text-danger"></i></a></td>
                                </tr>
                               <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
        <!-- /.inner -->
    </div>
    <!-- /.outer -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer_scripts'); ?>
    <!--Plugin scripts-->
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/select2/js/select2.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/datatables/js/jquery.dataTables.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/datatables/js/dataTables.bootstrap.min.js')); ?>"></script>
    <script type="text/javascript"
            src="<?php echo e(asset('assets/vendors/datatables/js/dataTables.responsive.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/datatables/js/dataTables.buttons.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/datatables/js/buttons.colVis.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/datatables/js/buttons.html5.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/datatables/js/buttons.bootstrap.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/datatables/js/buttons.print.min.js')); ?>"></script>
    <!--End of plugin scripts-->
    <!--Page level scripts-->
    <script type="text/javascript" src="<?php echo e(asset('assets/js/pages/users.js')); ?>"></script>
    <!-- end page level scripts -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts/default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>