            <ul id="menu">
                   <li <?php echo (Request::is('index')? 'class="active"':""); ?>>
                        <a href="<?php echo e(URL::to('admin//')); ?> ">
                            <i class="fa fa-home"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Dashboard</span>
                        </a>
                    </li>
                      <li  <?php echo (Request::is('category') || Request::is('addcategory') ? 'class="active"' : ''); ?>>
                        <a href="javascript:;">
                            <i class="fa fa-bar-chart"></i>
                            <span class="link-title menu_hide">&nbsp; Category</span>
                            <span class="fa arrow menu_hide"></span>
                        </a>
                        <ul>
                            <li <?php echo (Request::is('category') ? 'class="active"' : ''); ?>>
                                <a href="<?php echo e(URL::to('admin/category')); ?>">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Category
                                </a>
                            </li>
                             <li <?php echo (Request::is('addcategory') ? 'class="active"' : ''); ?>>
                                <a href="<?php echo e(URL::to('admin/addcategory')); ?>">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Add Category
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li  <?php echo (Request::is('addproduct') || Request::is('product') ? 'class="active"' : ''); ?>>
                        <a href="javascript:;">
                            <i class="fa fa-product-hunt"></i>
                            <span class="link-title menu_hide">&nbsp; Product</span>
                            <span class="fa arrow menu_hide"></span>
                        </a>
                        <ul>
                            <li <?php echo (Request::is('product') ? 'class="active"' : ''); ?>>
                                <a href="<?php echo e(URL::to('admin/product')); ?>">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Product List
                                </a>
                            </li>
                             <li <?php echo (Request::is('addproduct') ? 'class="active"' : ''); ?>>
                                <a href="<?php echo e(URL::to('admin/addproduct')); ?>">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Add Product
                                </a>
                            </li>

                        </ul>
                    </li>

                     <li <?php echo (Request::is('gallery')? 'class="active"':""); ?>>
                        <a href="<?php echo e(URL::to('admin/gallery')); ?> ">
                            <i class="fa fa-picture-o"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Gallery</span>
                        </a>
                    </li>

                     <li <?php echo (Request::is('gallery')? 'class="active"':""); ?>>
                        <a href="<?php echo e(URL::to('admin/video')); ?> ">
                            <i class="fa fa-youtube-play"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Vedio Gallery</span>
                        </a>
                    </li>




                    <li  <?php echo (Request::is('addpages') || Request::is('pages') ? 'class="active"' : ''); ?>>
                        <a href="javascript:;">
                            <i class="fa fa-product-hunt"></i>
                            <span class="link-title menu_hide">&nbsp; Pages</span>
                            <span class="fa arrow menu_hide"></span>
                        </a>
                        <ul>
                            <li <?php echo (Request::is('pages') ? 'class="active"' : ''); ?>>
                                <a href="<?php echo e(URL::to('admin/pages')); ?>">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Pages 
                                </a>
                            </li>
                             <li <?php echo (Request::is('addpages') ? 'class="active"' : ''); ?>>
                                <a href="<?php echo e(URL::to('admin/addpages')); ?>">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Add Pages
                                </a>
                            </li>

                        </ul>
                    </li>

                     <li <?php echo (Request::is('setting')? 'class="active"':""); ?>>
                        <a href="<?php echo e(URL::to('admin/setting')); ?> ">
                            <i class="fa fa-cogs"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Setting</span>
                        </a>
                    </li>

                     <li <?php echo (Request::is('lead')? 'class="active"':""); ?>>
                        <a href="<?php echo e(URL::to('admin/lead')); ?> ">
                            <i class="fa fa-users"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;lead</span>
                        </a>
                    </li>

              
                </ul>