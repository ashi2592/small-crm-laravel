<?php $__env->startSection('title'); ?>
    Gallery
    ##parent-placeholder-3c6de1b7dd91465d437ef415f94f36afc1fbc8a8##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header_styles'); ?>
    <!--Plugin styles -->
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/fancybox/css/jquery.fancybox.css')); ?>"/>
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/fancybox/css/jquery.fancybox-buttons.css')); ?>" />
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/fancybox/css/jquery.fancybox-thumbs.css')); ?>" />
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/imagehover/css/imagehover.min.css')); ?>" />
    <!--End of plugin-->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/css/pages/gallery.css')); ?>" />

         <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/fileinput/css/fileinput.min.css')); ?>"/>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <header class="head">
        <div class="main-bar">
            <div class="row no-gutters">
                <div class="col-lg-6 col-md-4 col-sm-4">
                    <h4 class="nav_top_align">
                        <i class="fa fa-image"></i>
                        Gallery
                    </h4>
                </div>
                <div class="col-lg-6 col-md-8 col-sm-8">
                    <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                        <li class="breadcrumb-item">
                            <a href="index1">
                                <i class="fa fa-home" data-pack="default" data-tags=""></i>
                                Dashboard
                            </a>
                        </li>
                        <li class="breadcrumb-item active">Gallery</li>
                    </ol>
                </div>
            </div>
        </div>
    </header>

<div class="outer">
        <div class="inner bg-light lter bg-container">
     <form class="form-horizontal" action="<?php echo e(URL::to('admin/uploadgallery')); ?>" method="post">
             <?php echo e(csrf_field()); ?>

            <div class="multipleImgeImput">

            </div>
             <div class="row summer_note_display summer_note_btn">
                <div class="col-12">
                    <div class='card m-t-35'>
                        <div class='card-header bg-white '>
                            Upload Image
                            <!-- tools box -->
                            <div class="float-right box-tools"></div>
                            <!-- /. tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class='card-block pad m-t-25'>
                              <input id="input-21" type="file"  class="file-loading" multiple >
                         </div>

                        <div class="form-group row">
                        <div class="col-lg-1 push-lg-11">
                                 <button class="btn btn-primary ">Save</button>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
         </form>
     </div>
 </div>

    <div class="outer">
        <div class="inner bg-light lter bg-container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header bg-white">
                            Gallery
                        </div>
                        <div class="card-block m-t-35">
                            <div>
                                <div class="nav-tabs-custom">
                                    <ul class="nav nav-tabs">
                                        <li class="nav-item">
                                            
                                        </li>
                                     
                                       
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active gallery-padding" id="tab_2">
                                            <div class="row no-gutters">
                                                <?php if(count($data)) : 
                                                foreach ($data as $key => $value):
                                                 ?>
                                                <div class="col-xl-2 col-lg-3 col-md-4 col-6 gallery-border">
                                                    <a class="fancybox zoom thumb_zoom" href="<?php echo e(asset($value['image'])); ?>" data-fancybox-group="gallery" title="Image Title <?php echo e($value['id']); ?>">
                                                        <img src="<?php echo e(asset($value['image'])); ?>" class="img-fluid gallery-style" alt="Image"></a>
                                                       
                                                         <button class="btn btn-danger" 

                                                         onclick="deleteImg(<?php echo $value['id']; ?>)"><i class="fa fa-trash"></i> Delete</button>
                                                </div>

                                              <?php endforeach; endif ; ?>
                                                
                                            </div>
                                        </div>
                                       
                                    </div>
                                    <!-- /.tab-content -->
                                </div>
                                <!-- nav-tabs-custom -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.inner -->
    </div>

    <!-- /.outer -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer_scripts'); ?>
    <!--Plugin scripts-->
    <!--Plugin scripts-->
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/holderjs/js/holder.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/fancybox/js/jquery.fancybox.pack.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/fancybox/js/jquery.fancybox-buttons.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/fancybox/js/jquery.fancybox-thumbs.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/fancybox/js/jquery.fancybox-media.js')); ?>"></script>
    <!--End of plugin scripts-->
    <script type="text/javascript" src="<?php echo e(asset('assets/js/pages/gallery.js')); ?>"></script>

    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/fileinput/js/fileinput.min.js')); ?>"></script>

 <script type="text/javascript">

          $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });

           $("#input-21").fileinput({
                theme: "fa",
                previewFileType: "image",
                browseClass: "btn btn-success",
                browseLabel: "Pick Image",
                removeClass: "btn btn-danger",
                removeLabel: "Delete",
                showPreview: true,
                allowedFileExtensions: ['jpg', 'png', 'gif'],
                maxFileCount: 10,
                uploadUrl: 'uploadImage',
                initialPreview: [],
               
                initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
                initialPreviewFileType: 'image', // image is the default and can be overridden in config below
                }).on('fileuploaded', function(event,data,previewId, index) {
                   
                     var response = (data.response);
                   // alert(response.filename);

                    var inputHtml = '<input type="hidden" name="image[]"  value="'+response.filename+'"/>'
                    $('.multipleImgeImput').append(inputHtml);
                });

                function deleteImg(id)
                {
                    $.ajax({
                        url: "deleteImages?id="+id,
                        type:'get',
                        success: function(res)
                        {
                            location.reload();
                        }

                    })
                }

        </script>
   
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts/default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>