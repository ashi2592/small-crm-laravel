<?php $__env->startSection('title'); ?>
    Category
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header_styles'); ?>
    <!--plugin styles-->
    <link rel="stylesheet" href="<?php echo e(asset('assets/vendors/intl-tel-input/css/intlTelInput.css')); ?>">
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')); ?>" />
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/sweetalert/css/sweetalert2.min.css')); ?>" />
    <!--End of plugin styles-->
    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/css/pages/sweet_alert.css')); ?>" />
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/css/pages/form_layouts.css')); ?>" />
    <!-- end of page level styles -->

    <!-- global styles-->
    <link type="text/css" rel="stylesheet" media="screen" href="<?php echo e(asset('assets/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css')); ?>"/>
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/summernote/css/summernote.css')); ?>"/>
    <!-- end of global styles-->
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/css/pages/form_elements.css')); ?>"/>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <header class="head">
        <div class="main-bar">
            <div class="row no-gutters">
                <div class="col-sm-5 col-lg-6 skin_txt">
                    <h4 class="nav_top_align">
                        <i class="fa fa-pencil"></i>
                        Add Category
                    </h4>
                </div>
                <div class="col-sm-7 col-lg-6">
                    <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                        <li class="breadcrumb-item">
                            <a href="index1">
                                <i class="fa fa-home" data-pack="default" data-tags=""></i> Dashboard
                            </a>
                        </li>
                       
                        <li class="active breadcrumb-item"> Add Category</li>
                    </ol>
                </div>
            </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
              
                <!-- basic sign up form-->
                <div class="col-12 col-xl-6">
                    <div class="card m-t-35">
                        <div class="card-header bg-white">
                            Add Category
                        </div>
                        <div class="card-block">
                            <form class="form-horizontal" action="<?php echo e(URL::to('admin/addcategory')); ?>" method="post">
                                <?php echo e(csrf_field()); ?>

                                <fieldset>
                                    <!-- Name input-->
                                    <div class="form-group row m-t-35">
                                        <div class="col-lg-3 text-lg-right">
                                            <label for="name" class="col-form-label">Name</label>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="input-group">
                                                            <span class="input-group-addon">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                <input type="text" name="name" id="name" class="form-control" placeholder="Category Name" required=""  value="<?php echo e(isset($result['product_group'])?$result['product_group']:''); ?>" >
                                            </div>
                                        </div>
                                    </div>
                                    <!-- first name-->
                                    <div class="form-group row">
                                        <div class="col-lg-3 text-lg-right">
                                            <label for="slug" class="col-form-label">Slug</label>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                <input type="text" name="slug" class="form-control" id="slug" placeholder="Slug" required value="<?php echo e(isset($result['product_group_slug'])?$result['product_group_slug']:''); ?>">
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <!-- last name-->
                                    <div class="form-group row">
                                        <div class="col-lg-3 text-lg-right">
                                            <label for="order" class="col-form-label">Sort Order</label>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="input-group">
                                                            <span class="input-group-addon">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                <input type="text" id="order" name="order" class="form-control" placeholder="Sort Order" value="<?php echo e(isset($result['order'])?$result['order']:0); ?>" required>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- mail name-->
                                   
                                   
                                    <!-- re password name-->
                                    <div class="form-group row">
                                        <div class="col-lg-3 text-lg-right">
                                            <label for="status" class="col-form-label">Status</label>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="input-group">
                                                            <span class="input-group-addon">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                <select class="form-control" id="status" name="status" required="" >
                                                    <option value="">Select</option>
                                                    <option value="1" <?php echo e((isset($result['status']) && $result['status'] == 1)?'selected':0); ?> >Active</option>
                                                    <option value="2"  <?php echo e((isset($result['status']) && $result['status'] == 2 )?'selected':0); ?>>Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- last name-->
                                    <div class="form-group row">
                                        <div class="col-lg-9 push-lg-3">
                                            <button class="btn btn-primary ">Save</button>
                                            
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>

                    </div>
                   
                </div>

                <?php if(isset($_GET['id'])): ?>
                <!-- Horizontal sign up form-->
                  <div class="col-12 col-xl-6">
                     <div class="card m-t-35">
                        <div class="card-header bg-white">
                            Seo Details
                        </div>
                        <div class="card-block">
                            <form class="form-horizontal" action="<?php echo e(URL::to('admin/addcategory')); ?>" method="post">
                                   <?php echo e(csrf_field()); ?>

                                      <input type="hidden" name="id" value="<?php echo e($_GET['id']); ?>">
                                <fieldset>
                                    <!-- Name input-->
                                    <div class="form-group row m-t-35">
                                        <div class="col-lg-3 text-lg-right">
                                            <label for="name" class="col-form-label">Meta Descriptions</label>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="input-group">
                                                   <textarea id="autosize" class="form-control" cols="50" rows="5" name="meta_desc"><?php echo e(isset($result['meta_desc'])?$result['meta_desc']:''); ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- first name-->
                                    <div class="form-group row m-t-35">
                                        <div class="col-lg-3 text-lg-right">
                                            <label for="name" class="col-form-label">Keywords</label>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="input-group">
                                                   <textarea id="autosize" class="form-control" cols="50" rows="5" name="product_keyword"><?php echo e(isset($result['product_keyword'])?$result['product_keyword']:''); ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <div class="form-group row">
                                        <div class="col-lg-9 push-lg-3">
                                            <button class="btn btn-primary ">Save</button>
                                            
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>

                    </div>
                </div>
                <?php endif; ?>
                <!-- end of horizontal signin layout-->
            </div>

            <?php if(isset($_GET['id'])): ?>
            <form class="form-horizontal" action="<?php echo e(URL::to('admin/addcategory')); ?>" method="post">
                                <?php echo e(csrf_field()); ?>

            <input type="hidden" name="id" value="<?php echo e($_GET['id']); ?>">

             <div class="row summer_note_display summer_note_btn">
                <div class="col-12">
                    <div class='card m-t-35'>
                        <div class='card-header bg-white '>
                            Description
                            <!-- tools box -->
                            <div class="float-right box-tools"></div>
                            <!-- /. tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class='card-block pad m-t-25'>
                           
                                    <textarea class="textarea form_editors_textarea_wysihtml"
                                              placeholder="Place some text here" name="product_description"><?php echo e(isset($result['product_description'])?$result['product_description']:''); ?></textarea>
                         </div>

                        <div class="form-group row">
                        <div class="col-lg-9 push-lg-3">
                                 <button class="btn btn-primary ">Save</button>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
         </form>
         <?php endif; ?>
        </div>
        <!-- /.inner -->
    </div>
    <!-- /.outer -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer_scripts'); ?>
    <!--Plugin scripts-->
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/intl-tel-input/js/intlTelInput.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/sweetalert/js/sweetalert2.min.js')); ?>"></script>
    <!--End of Plugin scripts-->
    <!--Page level scripts-->
    <script type="text/javascript" src="<?php echo e(asset('assets/js/pages/form_layouts.js')); ?>"></script>
    <!-- end of page level js -->
     <!--Plugin scripts-->
 
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/bootstrap3-wysihtml5-bower/js/bootstrap3-wysihtml5.all.min.js')); ?>"></script>
    <!--Page level scripts-->
    <script type="text/javascript" src="<?php echo e(asset('assets/js/pages/form_editors.js')); ?>"></script>
    <!-- end page level scripts -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts/default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>