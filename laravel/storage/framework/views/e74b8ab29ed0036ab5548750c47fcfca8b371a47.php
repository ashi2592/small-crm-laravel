<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title>
        <?php $__env->startSection('title'); ?>
            | Product
        <?php echo $__env->yieldSection(); ?>
    </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo e(asset('assets/img/logo1.ico')); ?>"/>
    <!-- global styles-->
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/css/components.css')); ?>"/>
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/css/custom.css')); ?>"/>
    <link type="text/css" rel="stylesheet" href="#" id="skin_change"/>
    <!-- end of global styles-->
    <?php echo $__env->yieldContent('header_styles'); ?>
</head>

<body class="fixedNav_position">
<div class="preloader" style=" position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  z-index: 100000;
  backface-visibility: hidden;
  background: #ffffff;">
    <div class="preloader_img" style="width: 200px;
  height: 200px;
  position: absolute;
  left: 48%;
  top: 48%;
  background-position: center;
z-index: 999999">
        <img src="<?php echo e(asset('assets/img/loader.gif')); ?>" style=" width: 40px;" alt="loading...">
    </div>
</div>
<div id="wrap">
    <div id="top" class="fixed">
        <!-- .navbar -->
        <nav class="navbar navbar-static-top">
            <div class="container-fluid m-0">
                <a class="navbar-brand float-left" href="index">
                    <h4><img src="<?php echo e(asset('assets/img/logo1.ico')); ?>" class="admin_img" alt="logo"> Admin</h4>
                </a>
                <div class="menu">
                    <span class="toggle-left" id="menu-toggle">
                        <i class="fa fa-bars"></i>
                    </span>
                </div>
                <div class="topnav dropdown-menu-right float-right">
                    <div class="btn-group hidden-md-up small_device_search" data-toggle="modal"
                         data-target="#search_modal">
                        <i class="fa fa-search text-primary"></i>
                    </div>
                    
                    <div class="btn-group">
                        <div class="user-settings no-bg">
                            <button type="button" class="btn btn-default no-bg micheal_btn" data-toggle="dropdown">
                                <img src="<?php echo e(asset('assets/img/admin.jpg')); ?>" class="admin_img2 img-thumbnail rounded-circle avatar-img"
                                     alt="avatar"> <strong><?php echo e(Auth::user()->name); ?></strong>
                                <span class="fa fa-sort-down white_bg"></span>
                            </button>
                            <div class="dropdown-menu admire_admin">
                                
                                <a class="dropdown-item" href="lockscreen"><i class="fa fa-lock"></i>
                                    Lock Screen</a>
                                <a class="dropdown-item" href="<?php echo e(route('logout')); ?>"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i>
                                    Log Out</a>
                                    <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                    <?php echo e(csrf_field()); ?>

                                    </form>

                            </div>
                        </div>
                    </div>

                </div>
                
            </div>
            <!-- /.container-fluid -->
        </nav>
        <!-- /.navbar -->
        <!-- /.head -->
    </div>
    <!-- /#top -->
    <div class="wrapper fixedNav_top">
        <div id="left">
            <div class="menu_scroll">
                <div class="left_media">
                    <div class="media user-media">
                        <div class="user-media-toggleHover">
                            <span class="fa fa-user"></span>
                        </div>
                        <div class="user-wrapper">
                            <a class="user-link" href="#">
                                <img class="media-object img-thumbnail user-img rounded-circle admin_img3" alt="User Picture"
                                     src="<?php echo e(asset('assets/img/admin.jpg')); ?>">
                                <p class="user-info menu_hide">Welcome <?php echo e(Auth::user()->name); ?></p>
                            </a>
                        </div>
                    </div>
                    <hr/>
                </div>
               <?php echo $__env->make('sections.leftmenu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <!-- /#menu -->
            </div>
        </div>
        <!-- /#left -->

        <div id="content" class="bg-container">
            <!-- Content -->
        <?php echo $__env->yieldContent('content'); ?>
        <!-- Content end -->
        </div>
        <div class="modal fade" id="search_modal" tabindex="-1" role="dialog"
             aria-hidden="true">
            <form>
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span class="float-right" aria-hidden="true">&times;</span>
                        </button>
                       
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /#content -->
    <!-- <?php echo $__env->make('layouts.right_sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> -->

</div>
<!-- /#wrap -->
<!-- global scripts-->
<script type="text/javascript" src="<?php echo e(asset('assets/js/components.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('assets/js/custom.js')); ?>"></script>
<!-- end of global scripts-->
<!-- page level js -->
<?php echo $__env->yieldContent('footer_scripts'); ?>
<!-- end page level js -->
<script type="text/javascript">
    var  toogle= localStorage.getItem('tooglesidebar');
    console.log(toogle);
    if(toogle)
    {
         $(".fixedNav_position").removeClass('sidebar-left-hidden');
    }
    else{
      
        $(".fixedNav_position").addClass('sidebar-left-hidden')
    }

    $(".fixedNav_position").on('click',function(){
        var  toogle= localStorage.getItem('tooglesidebar');
        if(toogle == 1)
        {
            localStorage.removeItem('tooglesidebar');
        }    
        else{
            localStorage.setItem('tooglesidebar', 1); 

        }
    })
</script>
</body>
</html>