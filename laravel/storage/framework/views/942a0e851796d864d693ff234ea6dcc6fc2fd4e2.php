<?php $__env->startSection('title'); ?>
    <?php echo e($title); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('description',$meta_desc); ?>
<?php $__env->startSection('keywords',$keyword); ?>

<?php $__env->startSection('header_styles'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
   <!-- start of hero -->   
        <section class="her hero-slider-wrapper">
            <div class="her-slider">
                <div class="slide">
                    <img src="<?php echo e(asset('front/images/slider/about.jpg')); ?>" alt class="slider-bg">
                    
                </div>

            </div>
        </section>
        <!-- end of hero slider -->

  <!-- start of services-serction-s3 --> 
        <section class="services-serction-s3 section-padding">
          <div class="container">
                <div class="row">
                    <div class="col col-lg-8">
                         <div class="cat-1">Our Products:</div>
                        <div class="row services-s2-grids">
                          
                     <?php if(count($products)): ?>
                        <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="col col-lg-4 col-xs-6">
                            <div class="grid">
                                <div class="img-holder">
                                    <img src="<?php echo e(asset($product['image'])); ?>" alt class="img img-responsive">
                                </div>
                                <div class="details">
                                    <h3><a href="<?php echo e(url('/product')); ?>/<?php echo e($product['product_slug']); ?>"><?php echo e($product['product_name']); ?></a></h3>
                                    <p><?php echo htmlspecialchars_decode($product['product_desc']) ; ?></p>
                                    <a href="<?php echo e(url('/product')); ?>/<?php echo e($product['product_slug']); ?>" class="read-more"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Read More</a>
                                </div>
                            </div>
                        </div>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    <?php endif; ?>
                    
                </div>
                    </div>
                    <?php echo $__env->make('front.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                </div>
            </div>
           
                
            
                
                
            <!-- end container -->
        </section>
        <!-- end of services-serction-s3 -->


<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer_script'); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts/frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>