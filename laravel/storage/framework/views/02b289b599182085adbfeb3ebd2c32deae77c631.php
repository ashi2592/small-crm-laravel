<?php $__env->startSection('title'); ?>
    <?php echo e($title); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('description',$meta_desc); ?>
<?php $__env->startSection('keywords',$keyword); ?>


<?php $__env->startSection('header_styles'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
   <!-- start of hero -->   
        <section class="her hero-slider-wrapper">
            <div class="her-slider">
                <div class="slide">
                    <img src="<?php echo e(asset('front/images/slider/about.jpg')); ?>" alt class="slider-bg">
                    
                </div>

            </div>
        </section>
        <!-- end of hero slider -->

		<!-- start of services-serction-s3 --> 
		<section class="services-serction-s3 section-padding">
		<div class="container">
		<div class="row">
			<div class="col-lg-5  col-md-5">
				<div class="pro-1"> <img src="<?php echo e(asset($product['image'])); ?>" width="100%" class="img-responsive"/></div>
			</div>

		<div class="col col-lg-7  col-md-7 service-st2-content">
			<div class="service-single-content">
				<div class="service-details">
					<h2><?php echo e($product['product_name']); ?></h2>
					<p><?php echo htmlspecialchars_decode($product['product_desc']) ; ?></p>
					
					<h4>Product Details:</h4>


					<table>
					

						<thead>
							<tr>
							<th style="width:50%">Title</th>
							<th>Specifications</th>
							
						</tr>
						</thead>
						<tbody>
						<?php if(count($Spefictions) > 0 ): ?>
						<?php $__currentLoopData = $Spefictions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$spe): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<tr>
						<td><?php echo e($spe['title']); ?></td>
						<td><?php echo e($spe['descr']); ?></td>
						</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						<?php endif ; ?>
						</tbody>
						
						
					</table>



				</div>

			</div>
		</div>
		</div>
		</div>





		<!-- end container -->
		</section>
		<!-- end of services-serction-s3 -->




<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer_script'); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts/frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>