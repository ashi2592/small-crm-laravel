 <!-- start footer-->
        <footer class="site-footer">
            <div class="container">
                <div class="row">
                    <div class="col col-lg-4 col-md-3 col-xs-6">
                        <div class="widget about-widget">
                            <h3><a href="#" class="logo">About Our Industry</a></h3>
                            <p>Established in the year 2016, we “Ansh Enterprises” is the leading Manufacturer of Agarbatti Making Machine, Foil Container Making Machine, Plate Making Machine, Vacuum Forming Machine, etc. Our company has earned the trust of all its clients and gained a remarkable position in the market for excellent manufacturing of these products and this has also helped us in maintaining a long-term relationship with our clients</p>
                        </div>
                    </div>

                    <div class="col col-lg-2 col-md-3 col-xs-6">
                        <div class="widget site-map-widget">
                            <h3>Quick Link</h3>
                            <ul>
                                <li><a href="#">Home</a></li>
                                <li><a href="#">About</a></li>
                                
                                <li><a href="#">Blog</a></li>
                                <li><a href="#">Sitemap</a></li>
                                <li><a href="#">Contact</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col col-lg-3 col-md-3 col-xs-6">
                        <div class="widget news-widget">
                            <h3>Like Us</h3>
                            
                        </div>
                    </div>

                    <div class="col col-lg-3 col-md-3 col-xs-6">
                        <div class="widget newsletter-widget">
                            <h3>Enquiry Now</h3>
                           
                            
                        </div>

                        <div class="widget social-media-widget">
                            <ul class="social-links">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div> <!-- end row -->
            </div> <!-- end container -->
        </footer>
        <!-- end footer-->