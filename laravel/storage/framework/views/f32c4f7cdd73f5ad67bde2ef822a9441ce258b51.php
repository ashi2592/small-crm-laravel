<?php $__env->startSection('title'); ?>
    Dashboard-2
    ##parent-placeholder-3c6de1b7dd91465d437ef415f94f36afc1fbc8a8##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header_styles'); ?>
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/chartist/css/chartist.min.css')); ?>" />
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/circliful/css/jquery.circliful.css')); ?>">
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/css/pages/index.css')); ?>">

    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/swiper/css/swiper.min.css')); ?>"/>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <header class="head">
        <div class="main-bar">
            <div class="row no-gutters">
                <div class="col-6">
                    <h4 class="m-t-5">
                        <i class="fa fa-home"></i>
                        Dashboard
                    </h4>
                </div>
            </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">


            <!--top section widgets-->
            <div class="row widget_countup">
                <div class="col-12 col-sm-6 col-xl-3">

                    <div id="top_widget1">
                        <div class="front">
                            <div class="bg-primary p-d-15 b_r_5">
                                <div class="float-right m-t-5">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                                <div class="user_font">Product</div>
                                <div id="widget_countup1"><?php echo e(commonhelper::getproductCount()); ?></div>
                                <div class="tag-white">
                                    <!-- <span id="percent_count1">85</span>% -->
                                </div>
                                <div class="previous_font">All Product Count</div>
                            </div>
                        </div>
                        
                    </div>
                </div>

                <div class="col-12 col-sm-6 col-xl-3 media_max_573">
                    <div id="top_widget2">
                        <div class="front">
                            <div class="bg-success p-d-15 b_r_5">
                                <div class="float-right m-t-5">
                                    <i class="fa fa-users"></i>
                                </div>
                                <div class="user_font">Leads</div>
                                <div id="widget_countup3"><?php echo e(commonhelper::getLeadCount()); ?></div>
                                <div class="tag-white ">
                                    
                                </div>
                                <div class="previous_font">Generated Leads</div>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="col-12 col-sm-6 col-xl-3 media_max_1199">
                    <div id="top_widget3">
                        <div class="front">
                            <div class="bg-warning p-d-15 b_r_5">
                                <div class="float-right m-t-5">
                                    <i class="fa fa fa-picture-o"></i>
                                </div>
                                <div class="user_font">Images</div>
                                <div id="widget_countup3"><?php echo e(commonhelper::getGalleryCount()); ?></div>
                                <div class="tag-white ">
                                    
                                </div>
                                <div class="previous_font">Images For Gallery</div>
                            </div>
                        </div>

                    
                    </div>

                </div>
                <div class="col-12 col-sm-6 col-xl-3 media_max_1199">
                    <div id="top_widget4">
                        <div class="front">
                            <div class="bg-danger p-d-15 b_r_5">
                                <div class="float-right m-t-5">
                                    <i class="fa fa-tasks"></i>
                                </div>
                                <div class="user_font">Category</div>
                                <div id="widget_countup4"><?php echo e(commonhelper::getproductCount()); ?></div>
                                <div class="tag-white">
                                   <!--  <span id="percent_count4">80</span>% -->
                                </div>
                                <div class="previous_font">All Category</div>
                            </div>
                        </div>

                        
                    </div>

                </div>
            </div>

           
            <div class="row">
                <div class="col-lg-4 m-t-35">
                    <div class="bg-white section_border">
                        <h4 class="header_align">Products</h4><hr />
                        <div class="header_align">

                            <?php $products = commonhelper::getProducts(4,['product_name','image','price']); 
                                
                             ?>
                             <?php if(count($products)): ?>
                            
                           <!--  <h5 class="text-mint float-left">Price</h5>
                            <h5 class="text-danger float-right">$25</h5> -->
                            <div class="swiper-container men_shoes_swiper">
                                <div class="swiper-wrapper">
                                     <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <div class="swiper-slide">
                                        <h5 class="text-center float-left"><?php echo e($product['product_name']); ?></h5>
                                        <h5 class="text-danger float-right">Price: <?php echo e($product['price']); ?></h5>
                                        <br/>
                                        <img src="<?php echo e(asset($product['image'])); ?>" alt="Image missing" class="img-fluid"/>
                                      
                                        
                                    </div>
                                   
                                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                                <!-- Add Pagination -->
                                <div class="swiper-pagination"></div>
                            </div>
                           
                            <?php endif; ?>
                        </div>
                        
                    </div>
                </div>
             



                <div class="col-lg-5 col-12 m-t-35">
                    <div class="row">
                        <div class="col-12 text-center text-white">
                            <div class="lorem_background">
                                <div>
                                    <img src="<?php echo e(asset('assets/img/mailbox_imgs/2.jpg')); ?>" alt="lorem" class="img-fluid rounded-circle lorem_img">
                                </div>
                                <div class="text-white font_18">Stuart</div>
                            <!--<div>stuart@gmail.com</div>-->
                                <div class="text-center lorem_bg m-b-0">
                                    <div class="row">
                                        <div class="col-3 lorem_font_icon">
                                            <i class="fa fa-facebook"></i>
                                        </div>
                                        <div class="col-3 lorem_font_icon">
                                            <i class="fa fa-twitter"></i>
                                        </div>
                                        <div class="col-3 lorem_font_icon">
                                            <i class="fa fa-google-plus"></i>
                                        </div>
                                        <div class="col-3">
                                            <i class="fa fa-instagram"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="list-group bg-white section_border">
                                <a href="#" class="lorem_group_item lorem_group_item_bottom">
                                    <span class="badge badge-pill badge-primary float-right">224</span>
                                    <span class="p-l-10">Followers</span>
                                    <span class="float-left">
                                                <i class="fa fa-user"></i>
                                            </span>
                                </a>
                                <a href="#" class="lorem_group_item">
                                    <span class="badge badge-pill badge-primary float-right">14</span>
                                    <span class="p-l-10">Following</span>
                                    <span class="float-left">
                                                <i class="fa fa-users"></i>
                                            </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer_scripts'); ?>
    <!--  plugin scripts -->
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/countUp.js/js/countUp.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/flip/js/jquery.flip.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/js/pluginjs/jquery.sparkline.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/chartist/js/chartist.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/js/pluginjs/chartist-tooltip.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/swiper/js/swiper.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/circliful/js/jquery.circliful.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/flotchart/js/jquery.flot.js')); ?>" ></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/flotchart/js/jquery.flot.resize.js')); ?>"></script>
    <!--end of plugin scripts-->

    <script type="text/javascript" src="<?php echo e(asset('assets/js/pages/index.js')); ?>"></script>
        <script type="text/javascript" src="<?php echo e(asset('assets/vendors/swiper/js/swiper.min.js')); ?>"></script>
   <script type="text/javascript">
           // swiper
    var swiper = new Swiper('.men_shoes_swiper', {
        centeredSlides: true,
        autoplay: 2000,
        loop: true,
        autoplayDisableOnInteraction: false


    });

   </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts/default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>