  <!-- Start header -->
        <header class="site-header header-style-3">
            <div class="topbar">
                <div class="container">
                    <div class="row">
                        <div class="col col-sm-6 contact-info">
                            <ul>
                                <li><i class="fa fa-envelope-o" aria-hidden="true"></i>  nishant.unicon2326673@gmail.com</li>
                                <li><i class="fa fa-volume-control-phone" aria-hidden="true"></i>  +91-9999868419</li>
                            </ul>
                        </div>
                        <div class="col col-sm-6 language-login-wrapper">
                            <div class="language-login clearfix">
                                <div class="client-login">
                                    <a href="#" id="client-login-btn"><i class="fa fa-key" aria-hidden="true"></i> Enquiry Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- end topbar -->

            <nav class="navigation navbar navbar-default">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="open-btn">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="index.html"><img src="<?php echo e(asset('front/images/logo-2.png')); ?> " alt="logo"></a>
                    </div>

                    <div id="navbar" class="navbar-collapse collapse navbar-right navigation-holder">
                        <button class="close-navbar"><i class="fa fa-close"></i></button>
                        <ul class="nav navbar-nav">
                            <li class="menu-item-has-children current-menu-ancestor current-menu-parent">
                                <a href="<?php echo e(url('/')); ?>">Home</a>
                         
                            </li>
                            <li><a href="<?php echo e(url('/aboutus')); ?>">About</a></li>
                            <li class="menu-item-has-children">
                                <a href="#">Our Products</a>
                                <?php $category = commonhelper::getCategorys(); ?>
                                <ul class="sub-menu">
                                    <?php if(count($category)): ?>
                                      <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cate): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                            <li><a href="<?php echo e(url('category')); ?>/<?php echo e($cate['product_group_slug']); ?>"><?php echo e($cate['product_group']); ?></a></li>
                                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>                                  
                                </ul>
                            </li>
                             <li class="menu-item-has-children">
                                <a href="#">Our Gallery</a>
                                <ul class="sub-menu">
                                    <li><a href="<?php echo e(url('/gallery')); ?>">Images Gallery</a></li>
                                    <li><a href="<?php echo e(url('/gallery')); ?>">Video Gallery</a></li>                                                                                                      
                                </ul>
                            </li>
                            
                           
                            <li><a href="<?php echo e(url('/contact')); ?>">Contact</a></li>
                        </ul>
                    </div><!-- end of nav-collapse -->

                    <div class="social-links-holder">
                        <ul class="social-links">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-rss"></i></a></li>
                        </ul>
                    </div>
                </div><!-- end of container -->
            </nav>
        </header>
        <!-- end of header -->