<?php $__env->startSection('title'); ?>
    video  gallery
<?php $__env->stopSection(); ?>
<?php $__env->startSection('description','gallery'); ?>
<?php $__env->startSection('keywords','gallery'); ?>

<?php $__env->startSection('header_styles'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
   <!-- start of hero -->   
        <section class="her hero-slider-wrapper">
            <div class="her-slider">
                <div class="slide">
                    <img src="<?php echo e(asset('front/images/slider/about.jpg')); ?>" width="100%" alt class="slider-bg">
                </div>
            </div>
        </section>
        <!-- end of hero slider -->

  <!-- start of services-serction-s3 --> 
        <section class="services-serction-s3 section-padding">
          <div class="container">

<h2 align="center">OUR GALLERY</h2><br>
                <div class="row">
                        <?php if(count($data)) : 
                        foreach ($data as $key => $value):
                        ?>
                            <div class="col-md-3">
                            <div class="row">
                            <div class="gal-1">
                            <?php echo $value['youtube_link']; ?>
                            </div>
                            </div></div>

                        <?php endforeach; endif ; ?>

                
                </div>
            </div>
           
                
            
                
                
            <!-- end container -->
        </section>
        <!-- end of services-serction-s3 -->


<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer_script'); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts/frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>