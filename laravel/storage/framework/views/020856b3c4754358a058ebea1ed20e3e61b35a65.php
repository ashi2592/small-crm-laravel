 <!-- start of testimonials-s2-faq --> 
        <section class="testimonials-s2-faq section-padding">
            <div class="container">
                <div class="row">
                    <div class="col col-lg-6">
                        <div class="section-title-s7">
                            <h2>Clients’ <span>Testimonials</span></h2>
                        </div>
                        <div class="testimonials-slider-s2 dots-style-1">
                            <div class="grid">
                                <div class="client-quote">
                                    <p>"I am Vijay Anand I had a Deal with Ansh Enterprises , Delhi on 23rd February-2018 as I Have purchased Fully Automatic Paper Plate Making Machine with Mould and Dies from their Factory in Delhi at a reasonable Price, over all Good Experience with their Team.".</p>
                                </div>
                                <div class="client-info">
                                    <div class="client-pic">
                                        <img src="<?php echo e(asset('front/images/testimonials/img-1.jpg')); ?>" alt="">
                                    </div>
                                    <div class="client-details">
                                        <h4>Vijay Anand</h4>
                                        <span>Local Business</span>
                                    </div>
                                </div>
                            </div>
                            <div class="grid">
                                <div class="client-quote">
                                    <p>It was a Great Deal with Ansh Enterprises Company as I bought Thermocol Plate Making Machine from their Manufacturing unit inDelhi , I found Best Quality and Delivery from them and that show their professionalism.</p>
                                </div>
                                <div class="client-info">
                                    <div class="client-pic">
                                        <img src="<?php echo e(asset('front/images/testimonials/img-1.jpg')); ?>" alt="">
                                    </div>
                                    <div class="client-details">
                                        <h4>Raj Pratap Singh</h4>
                                        <span>Local Business</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col col-lg-6 faq-col">
                        <div class="section-title-s7">
                            <h2><span>FAQ</span></h2>
                        </div>
                        <div class="panel-group faq-accordion theme-accordion-s2" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true">What are the main changes to the Construction?</a>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <p>perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Criteria requiring appointments of Project.</a>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">What are the main changes to the Construction?</a>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi.</p>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </div>
                </div> <!-- end row -->
            </div> <!-- end container -->
        </section>
        <!-- end of testimonials-s2-faq -->