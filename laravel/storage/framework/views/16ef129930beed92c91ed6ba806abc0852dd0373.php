            <ul id="menu">
                   <li <?php echo (Request::is('index')? 'class="active"':""); ?>>
                        <a href="<?php echo e(URL::to('/')); ?> ">
                            <i class="fa fa-home"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Dashboard</span>
                        </a>
                    </li>
                      <li  <?php echo (Request::is('category') || Request::is('addcategory') ? 'class="active"' : ''); ?>>
                        <a href="javascript:;">
                            <i class="fa fa-bar-chart"></i>
                            <span class="link-title menu_hide">&nbsp; Category</span>
                            <span class="fa arrow menu_hide"></span>
                        </a>
                        <ul>
                            <li <?php echo (Request::is('category') ? 'class="active"' : ''); ?>>
                                <a href="<?php echo e(URL::to('category')); ?>">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Category
                                </a>
                            </li>
                             <li <?php echo (Request::is('addcategory') ? 'class="active"' : ''); ?>>
                                <a href="<?php echo e(URL::to('addcategory')); ?>">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Add Category
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li  <?php echo (Request::is('addproduct') || Request::is('product') ? 'class="active"' : ''); ?>>
                        <a href="javascript:;">
                            <i class="fa fa-product-hunt"></i>
                            <span class="link-title menu_hide">&nbsp; Product</span>
                            <span class="fa arrow menu_hide"></span>
                        </a>
                        <ul>
                            <li <?php echo (Request::is('product') ? 'class="active"' : ''); ?>>
                                <a href="<?php echo e(URL::to('product')); ?>">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Product List
                                </a>
                            </li>
                             <li <?php echo (Request::is('addproduct') ? 'class="active"' : ''); ?>>
                                <a href="<?php echo e(URL::to('addproduct')); ?>">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Add Product
                                </a>
                            </li>

                        </ul>
                    </li>

                     <li <?php echo (Request::is('gallery')? 'class="active"':""); ?>>
                        <a href="<?php echo e(URL::to('gallery')); ?> ">
                            <i class="fa fa-picture-o"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Gallery</span>
                        </a>
                    </li>


                    <li  <?php echo (Request::is('addpages') || Request::is('pages') ? 'class="active"' : ''); ?>>
                        <a href="javascript:;">
                            <i class="fa fa-product-hunt"></i>
                            <span class="link-title menu_hide">&nbsp; Pages</span>
                            <span class="fa arrow menu_hide"></span>
                        </a>
                        <ul>
                            <li <?php echo (Request::is('pages') ? 'class="active"' : ''); ?>>
                                <a href="<?php echo e(URL::to('pages')); ?>">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Pages 
                                </a>
                            </li>
                             <li <?php echo (Request::is('addpages') ? 'class="active"' : ''); ?>>
                                <a href="<?php echo e(URL::to('addpages')); ?>">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Add Pages
                                </a>
                            </li>

                        </ul>
                    </li>


                

                    <li <?php echo (Request::is('index1')? 'class="active"':""); ?>>
                        <a href="<?php echo e(URL::to('view/index1')); ?> ">
                            <i class="fa fa-home"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Dashboard 1</span>
                        </a>
                    </li>
                    <li <?php echo (Request::is('index')? 'class="active"':""); ?>>
                        <a href="<?php echo e(URL::to('view/index')); ?> ">
                            <i class="fa fa-tachometer"></i>
                            <span class="link-title menu_hide">&nbsp;Dashboard 2
                            </span>
                        </a>
                    </li>
                    <li  <?php echo (Request::is('general_components')|| Request::is('cards')|| Request::is('izitoast')|| Request::is('transitions') || Request::is('buttons')|| Request::is('icons')|| Request::is('animations')|| Request::is('sliders')|| Request::is('notifications')|| Request::is('p_notify')|| Request::is('modal')|| Request::is('sortable')|| Request::is('sweet_alert')|| Request::is('cropper')|| Request::is('jstree')|| Request::is('grids_layout') ? 'class="active"' : ''); ?>>
                        <a href="javascript:;">
                            <i class="fa fa-anchor"></i>
                            <span class="link-title menu_hide">&nbsp; Components</span>
                            <span class="fa arrow menu_hide"></span>
                        </a>
                        <ul>
                            <li <?php echo (Request::is('general_components') ? 'class="active"' : ''); ?>>
                                <a href="<?php echo e(URL::to('view/general_components')); ?>">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; General Components
                                </a>
                            </li>
                            <li <?php echo (Request::is('cards') ? 'class="active"' : ''); ?>>
                                <a href="<?php echo e(URL::to('view/cards')); ?>">
                                    <i class="fa fa-angle-right"></i>
                                    <span class="link-title">&nbsp;Cards<span class="tag tag-pill tag_warnm tag-success new_tag">New</span></span>
                                </a>
                            </li>
                            <li <?php echo (Request::is('transitions') ? 'class="active"' : ''); ?>>
                                <a href="<?php echo e(URL::to('view/transitions')); ?>">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Transitions
                                </a>
                            </li>
                            <li <?php echo (Request::is('buttons') ? 'class="active"' : ''); ?>>
                                <a href="<?php echo e(URL::to('view/buttons')); ?>">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Buttons
                                </a>
                            </li>
                            <li <?php echo (Request::is('icons') ? 'class="active"' : ''); ?>>
                                <a href="<?php echo e(URL::to('view/icons')); ?>">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Icons
                                </a>
                            </li>
                            <li <?php echo (Request::is('animations') ? 'class="active"' : ''); ?>>
                                <a href="<?php echo e(URL::to('view/animations')); ?>">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Animations
                                </a>
                            </li>

                            <li <?php echo (Request::is('sliders') ? 'class="active"' : ''); ?>>
                                <a href="<?php echo e(URL::to('view/sliders')); ?>">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Sliders
                                </a>
                            </li>
                            <li <?php echo (Request::is('notifications') ? 'class="active"' : ''); ?>>
                                <a href="<?php echo e(URL::to('view/notifications')); ?>">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Notifications
                                </a>
                            </li>
                            <li <?php echo (Request::is('p_notify') ? 'class="active"' : ''); ?>>
                                <a href="<?php echo e(URL::to('view/p_notify')); ?>">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; P-Notify
                                </a>
                            </li>
                            <li <?php echo (Request::is('izitoast') ? 'class="active"' : ''); ?>>
                                <a href="<?php echo e(URL::to('view/izitoast')); ?>">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Izi-Toast
                                </a>
                            </li>
                            <li <?php echo (Request::is('cropper') ? 'class="active"' : ''); ?>>
                                <a href="<?php echo e(URL::to('view/cropper')); ?>">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Cropper
                                </a>
                            </li>
                            <li <?php echo (Request::is('jstree') ? 'class="active"' : ''); ?>>
                                <a href="<?php echo e(URL::to('view/jstree')); ?>">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Js Tree
                                </a>
                            </li>
                            <li <?php echo (Request::is('modal') ? 'class="active"' : ''); ?>>
                                <a href="<?php echo e(URL::to('view/modal')); ?>">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Modals
                                </a>
                            </li>


                            <li <?php echo (Request::is('sortable') ? 'class="active"' : ''); ?>>
                                <a href="<?php echo e(URL::to('view/sortable')); ?>">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Sortable
                                </a>
                            </li>
                            <li <?php echo (Request::is('sweet_alert') ? 'class="active"' : ''); ?>>
                                <a href="<?php echo e(URL::to('view/sweet_alert')); ?>">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Sweet alerts
                                </a>
                            </li>
                            <li <?php echo (Request::is('grids_layout') ? 'class="active"' : ''); ?>>
                                <a href="<?php echo e(URL::to('view/grids_layout')); ?>">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Grid View
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li  <?php echo (Request::is ('widgets1')|| Request::is('widgets2')|| Request::is('widgets3')? 'class="active"':"" ); ?>>
                        <a href="javascript:;">
                            <i class="fa fa-th-large"></i>
                            <span class="link-title menu_hide">&nbsp; Widgets</span>
                            <span class="fa arrow menu_hide"></span>
                        </a>
                        <ul>
                            <li <?php echo (Request::is('widgets1')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/widgets1')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Widgets 1
                                </a>
                            </li>
                            <li <?php echo (Request::is('widgets2')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/widgets2')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Widgets 2
                                </a>
                            </li>
                            <li <?php echo (Request::is('widgets3')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/widgets3')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    <span class="link-title">&nbsp; Widgets 3 </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li   <?php echo (Request::is ('form_elements')|| Request::is('form_layouts')|| Request::is('form_validations')|| Request::is('form_editors')|| Request::is('radio_checkbox')|| Request::is('form_wizards')|| Request::is('datetime_picker')|| Request::is('ratings')? 'class="active"':"" ); ?>>
                        <a href="javascript:;">
                            <i class="fa fa-pencil"></i>
                            <span class="link-title menu_hide">&nbsp; Forms</span>
                            <span class="fa arrow menu_hide"></span>
                        </a>
                        <ul>
                            <li <?php echo (Request::is('form_elements')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/form_elements')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Form Elements
                                </a>
                            </li>
                            <li <?php echo (Request::is('form_layouts')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/form_layouts')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Form Layouts
                                </a>
                            </li>
                            <li <?php echo (Request::is('form_validations')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/form_validations')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Form Validations
                                </a>
                            </li>
                            <li <?php echo (Request::is('form_editors')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/form_editors')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Form Editors
                                </a>
                            </li>
                            <li <?php echo (Request::is('radio_checkbox')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/radio_checkbox')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Radio and Checkbox
                                </a>
                            </li>
                            <li <?php echo (Request::is('form_wizards')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/form_wizards')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Wizards
                                </a>
                            </li>
                            <li <?php echo (Request::is('datetime_picker')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/datetime_picker')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Date Time Picker
                                </a>
                            </li>
                            <li <?php echo (Request::is('ratings')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/ratings')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Ratings
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li  <?php echo (Request::is('simple_tables')|| Request::is('simple_datatables')|| Request::is('datatables') || Request::is('advanced_tables')? 'class="active"':""); ?>>
                        <a href="#">
                            <i class="fa fa-th"></i>
                            <span class="link-title menu_hide">&nbsp; Tables</span>
                            <span class="fa arrow menu_hide"></span>
                        </a>
                        <ul>
                            <li <?php echo (Request::is('simple_tables')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/simple_tables')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Simple Tables
                                </a>
                            </li>

                            <li <?php echo (Request::is('simple_datatables')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/simple_datatables')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    <span class="link-title">&nbsp; Simple Data Tables</span>

                                </a>
                            </li>
                            <li <?php echo (Request::is('datatables')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/datatables')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Data Tables
                                </a>
                            </li>
                            <li <?php echo (Request::is('advanced_tables')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/advanced_tables')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Advanced Tables
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li  <?php echo (Request::is('charts')|| Request::is('advanced_charts') || Request::is('chartist')|| Request::is('rickshaw')? 'class="active"':""); ?>>
                        <a href="#">
                            <i class="fa fa-bar-chart"></i>
                            <span class="link-title menu_hide">&nbsp; Charts</span>
                            <span class="fa arrow menu_hide"></span>
                        </a>
                        <ul>
                            <li <?php echo (Request::is('charts')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/charts')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Flot Charts
                                </a>
                            </li>
                            <li <?php echo (Request::is('advanced_charts')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/advanced_charts')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Advanced Charts
                                </a>
                            </li>
                            <li <?php echo (Request::is('chartist')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/chartist')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Chartist
                                </a>
                            </li>
                            <li <?php echo (Request::is('rickshaw')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/rickshaw')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Rickshaw Charts
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li <?php echo (Request::is('users')|| Request::is('add_user') || Request::is('view_user')|| Request::is('delete_user')? 'class="dropdown_menu active"':""); ?>>
                        <a href="#">
                            <i class="fa fa-user"></i>
                            <span class="link-title menu_hide">&nbsp; Users</span>
                            <span class="fa arrow menu_hide"></span>
                        </a>
                        <ul>
                            <li <?php echo (Request::is('users')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/users')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; User Grid
                                </a>
                            </li>
                            <li <?php echo (Request::is('add_user')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/add_user')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Add User
                                </a>
                            </li>
                            <li <?php echo (Request::is('view_user')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/view_user')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; User Profile
                                </a>
                            </li>
                            <li <?php echo (Request::is('delete_user')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/delete_user')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Delete User
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li <?php echo (Request::is('calendar')? 'class="active"':""); ?>>
                        <a href="<?php echo e(URL::to('view/calendar')); ?> ">
                            <i class="fa fa-calendar"></i>
                            <span class="link-title menu_hide">&nbsp; Calendar</span>
                            <span class="badge badge-pill badge-warning float-right calendar_badge menu_hide">7</span>
                        </a>
                    </li>
                    <li <?php echo (Request::is('gallery')? 'class="active"':""); ?>>
                        <a href="<?php echo e(URL::to('view/gallery')); ?> ">
                            <i class="fa fa-picture-o"></i>
                            <span class="link-title menu_hide"> Gallery</span>
                        </a>
                    </li>
                    <li  <?php echo (Request::is('mail_compose')|| Request::is('mail_inbox') || Request::is('mail_view')|| Request::is('mail_sent')|| Request::is('mail_spam')|| Request::is('mail_draft')|| Request::is('mail_trash') ? 'class="active"' : ''); ?>>
                        <a href="#">
                            <i class="fa fa-envelope-o"></i>
                            <span class="link-title menu_hide">&nbsp; Email</span>
                            <span class="fa arrow menu_hide"></span>
                        </a>
                        <ul>
                            <li <?php echo (Request::is('mail_compose')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/mail_compose')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Compose
                                </a>
                            </li>
                            <li <?php echo (Request::is('mail_inbox')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/mail_inbox')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Inbox
                                </a>
                            </li>
                            <li <?php echo (Request::is('mail_view')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/mail_view')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; View
                                </a>
                            </li>
                            <li <?php echo (Request::is('mail_sent')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/mail_sent')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Sent
                                </a>
                            </li>
                            <li <?php echo (Request::is('mail_spam')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/mail_spam')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Spam
                                </a>
                            </li>
                            <li <?php echo (Request::is('mail_draft')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/mail_draft')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Draft
                                </a>
                            </li>
                            <li <?php echo (Request::is('mail_trash')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/mail_trash')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Trash
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li  <?php echo (Request::is('maps')|| Request::is('jqvmaps') ? 'class="active"' : ''); ?>>
                        <a href="#">
                            <i class="fa fa-map-marker"></i>
                            <span class="link-title menu_hide">&nbsp; Maps</span>
                            <span class="fa arrow menu_hide"></span>
                        </a>
                        <ul>
                            <li <?php echo (Request::is('maps')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/maps')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Google Maps
                                </a>
                            </li>
                            <li <?php echo (Request::is('jqvmaps')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/jqvmaps')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Vector Maps
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li  <?php echo (Request::is('404')|| Request::is('500') || Request::is('login')|| Request::is('register')|| Request::is('lockscreen')|| Request::is('invoice')|| Request::is('blank') ? 'class="active"' : ''); ?>>
                        <a href="javascript:;">
                            <i class="fa fa-file"></i>
                            <span class="link-title menu_hide">&nbsp; Pages</span>
                            <span class="fa arrow menu_hide"></span>
                        </a>
                        <ul>
                            <li <?php echo (Request::is('404')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/404')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; 404
                                </a>
                            </li>
                            <li <?php echo (Request::is('500')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/500')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; 500
                                </a>
                            </li>
                            <li <?php echo (Request::is('login1')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/login1')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Login 1
                                </a>
                            </li>
                            <li <?php echo (Request::is('login2')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/login2')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Login 2
                                </a>
                            </li>
                            <li <?php echo (Request::is('login3')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/login3')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Login 3
                                </a>
                            </li>
                            <li <?php echo (Request::is('register1')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/register1')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Register 1
                                </a>
                            </li>
                            <li <?php echo (Request::is('register2')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/register2')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Register 2
                                </a>
                            </li>
                            <li <?php echo (Request::is('register3')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/register3')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Register 3
                                </a>
                            </li>
                            <li <?php echo (Request::is('lockscreen')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/lockscreen')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Lock Screen 1
                                </a>
                            </li>
                            <li <?php echo (Request::is('lockscreen2')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/lockscreen2')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Lock Screen 2
                                </a>
                            </li>
                            <li <?php echo (Request::is('lockscreen3')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/lockscreen3')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Lock Screen 3
                                </a>
                            </li>
                            <li <?php echo (Request::is('invoice')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/invoice')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Invoice
                                </a>
                            </li>
                            <li <?php echo (Request::is('blank')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/blank')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Blank Page
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li  <?php echo (Request::is('boxed')|| Request::is('fixed-header-boxed') || Request::is('fixed-header-menu')|| Request::is('fixed-header')|| Request::is('fixed-menu-boxed')|| Request::is('fixed-menu')|| Request::is('no-header')|| Request::is('no-left-sidebarr') ? 'class="active"' : ''); ?>>
                        <a href="javascript:;">
                            <i class="fa fa-th"></i>
                            <span class="link-title menu_hide">&nbsp; Layouts</span>
                            <span class="fa arrow menu_hide"></span>
                        </a>
                        <ul>
                            <li <?php echo (Request::is('boxed')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/boxed')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Boxed Layout
                                </a>
                            </li>
                            <li <?php echo (Request::is('fixed-header-boxed')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/fixed-header-boxed')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Boxed &amp; Fixed Header
                                </a>
                            </li>
                            <li <?php echo (Request::is('fixed-header-menu')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/fixed-header-menu')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Fixed Header &amp; Menu
                                </a>
                            </li>
                            <li <?php echo (Request::is('fixed-menu-boxed')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/fixed-menu-boxed')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Boxed &amp; Fixed Menu
                                </a>
                            </li>
                            <li <?php echo (Request::is('fixed-menu')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/fixed-menu')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; Fixed Menu
                                </a>
                            </li>
                            <li <?php echo (Request::is('no-header')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/no-header')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; No Header
                                </a>
                            </li>
                            <li <?php echo (Request::is('no-left-sidebar')? 'class="active"':""); ?>>
                                <a href="<?php echo e(URL::to('view/no-left-sidebar')); ?> ">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp; No Left Sidebar
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown_menu">
                        <a href="javascript:;">
                            <i class="fa fa-sitemap"></i>
                            <span class="link-title menu_hide">&nbsp; Multi Level Menu</span>
                            <span class="fa arrow menu_hide"></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="javascript:;">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Level 1
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="sub-menu sub-submenu">
                                    <li>
                                        <a href="javascript:;">
                                            <i class="fa fa-angle-right"></i>
                                            &nbsp;Level 2
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <i class="fa fa-angle-right"></i>
                                            &nbsp;Level 2
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <i class="fa fa-angle-right"></i>
                                            &nbsp;Level 2
                                            <span class="fa arrow"></span>
                                        </a>
                                        <ul class="sub-menu sub-submenu">
                                            <li>
                                                <a href="javascript:;">
                                                    <i class="fa fa-angle-right"></i>
                                                    &nbsp;Level 3
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <i class="fa fa-angle-right"></i>
                                                    &nbsp;Level 3
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <i class="fa fa-angle-right"></i>
                                                    &nbsp;Level 3
                                                    <span class="fa arrow"></span>
                                                </a>
                                                <ul class="sub-menu sub-submenu">
                                                    <li>
                                                        <a href="javascript:;">
                                                            <i class="fa fa-angle-right"></i>
                                                            &nbsp;Level 4
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;">
                                                            <i class="fa fa-angle-right"></i>
                                                            &nbsp;Level 4
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;">
                                                            <i class="fa fa-angle-right"></i>
                                                            &nbsp;Level 4
                                                            <span class="fa arrow"></span>
                                                        </a>
                                                        <ul class="sub-menu sub-submenu">
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="fa fa-angle-right"></i>
                                                                    &nbsp;Level 5
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="fa fa-angle-right"></i>
                                                                    &nbsp;Level 5
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="fa fa-angle-right"></i>
                                                                    &nbsp;Level 5
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <i class="fa fa-angle-right"></i>
                                                    &nbsp;Level 4
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <i class="fa fa-angle-right"></i>
                                            &nbsp;Level 2
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Level 1
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="fa fa-angle-right"></i>
                                    &nbsp;Level 1
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="sub-menu sub-submenu">
                                    <li>
                                        <a href="javascript:;">
                                            <i class="fa fa-angle-right"></i>
                                            &nbsp;Level 2
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <i class="fa fa-angle-right"></i>
                                            &nbsp;Level 2
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <i class="fa fa-angle-right"></i>
                                            &nbsp;Level 2
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>