<?php $__env->startSection('title'); ?>
    contact
<?php $__env->stopSection(); ?>
<?php $__env->startSection('description',''); ?>
<?php $__env->startSection('keywords',''); ?>

<?php $__env->startSection('header_styles'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
   <!-- start of hero -->   
        <section class="her hero-slider-wrapper">
            <div class="her-slider">
                <div class="slide">
                    <img src="<?php echo e(asset('front/images/slider/contact.jpg')); ?>" alt class="slider-bg">
                    
                </div>

            </div>
        </section>
        <!-- end of hero slider -->


        <!-- start of services-serction-s3 --> 
        <section class="services-serction-s3 section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <div class="contact-form">
                            <h3>Contact Form</h3>
                            <h3>Ansh Enterprises</h3>
                            <ul>
                                <li><i class="fa fa-home"></i> UG/F, C-404, Gali No. 17A, Bhajanpura, New Delhi - 110053, Delhi, India</li>
                                <li><i class="fa fa-phone"></i>+91-9999868419</li>
                            </ul>
                            <h4>Send Email</h4>
                            <form class="form contact-validation-active" id="contact-form" novalidate="novalidate">
                                <div>
                                    <input type="text" name="name" class="form-control" placeholder="Full Name">
                                </div><br>
                                <div>
                                    <input type="email" name="email" class="form-control" placeholder="Email">
                                </div><br>
                                <div>
                                    <input type="tel" name="phone" class="form-control" placeholder="Phone">
                                </div><br>
                                <div>
                                    <textarea name="query" class="form-control" placeholder="Message"></textarea>
                                </div><br>
                                
                                <div class="submit">
                                    <button type="submit">Send</button>
                                    <span id="loader"><img src="<?php echo e(asset('front/images/contact-ajax-loader.gif')); ?> " alt="Loader"></span>
                                </div><br>
                                <div class="error-handling-messages">
                                    <div id="success">Thank you</div>
                                    <div id="error"> Error occurred while sending email. Please try again later. </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
        
                
                <div>
           
                
                </div>
                
                
            </div> <!-- end container -->
        </section>
        <!-- end of services-serction-s3 -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer_script'); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts/frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>