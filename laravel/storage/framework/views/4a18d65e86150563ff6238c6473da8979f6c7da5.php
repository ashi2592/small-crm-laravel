<div class="client-login-area">

            <div class="client-login-form">
                <h3>Enquiry Now</h3>
                <form class="form" id="quickenquiry">
                     <div>
                        <label for="Name">Name</label>
                        <input type="text" id="name" name="name" class="form-control">
                    </div>
                    <div>
                        <label for="email">Email</label>
                        <input type="text" id="email" name="email" class="form-control">
                    </div>
                    <div>
                        <label for="Phone">Phone</label>
                        <input type="tel" id="phone" name="phone" class="form-control">
                    </div>
                    <div>
                        <label for="Phone">Query</label>
                        <textarea name="query" ></textarea>
                    </div>
                    <div>
                        <button type="submit">Submit</button>
                    <span id="loader2" class="hide"><img src="<?php echo e(asset('front/images/contact-ajax-loader.gif')); ?> " alt="Loader"></span>
                    <div class="error-handling-messages  hide">
                    <div id="success2">Thank you</div>
                    <div id="error2"> Error occurred while sending email. Please try again later. </div>
                    </div>
                    </div>
                </form>
            </div>
            >
        </div>