<?php $__env->startSection('title'); ?>
    Products
    ##parent-placeholder-3c6de1b7dd91465d437ef415f94f36afc1fbc8a8##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header_styles'); ?>
   <?php $img=isset($result['image'])?$result['image']:''; ?>

     <?php $productimg=isset($result['product_image'])?unserialize($result['product_image']):[]; 


         ?>

      



    <!--plugin styles-->
    <link rel="stylesheet" href="<?php echo e(asset('assets/vendors/intl-tel-input/css/intlTelInput.css')); ?>">
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')); ?>" />
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/sweetalert/css/sweetalert2.min.css')); ?>" />
    <!--End of plugin styles-->
    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/css/pages/sweet_alert.css')); ?>" />
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/css/pages/form_layouts.css')); ?>" />
    <!-- end of page level styles -->

    <!-- global styles-->
    <link type="text/css" rel="stylesheet" media="screen" href="<?php echo e(asset('assets/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css')); ?>"/>
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/summernote/css/summernote.css')); ?>"/>
    <!-- end of global styles-->
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/css/pages/form_elements.css')); ?>"/>
   
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/chosen/css/chosen.css')); ?>"/>
        <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/fileinput/css/fileinput.min.css')); ?>"/>
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/multiselect/css/multi-select.css')); ?>"/>
   
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <header class="head">
        <div class="main-bar">
            <div class="row no-gutters">
                <div class="col-sm-5 col-lg-6 skin_txt">
                    <h4 class="nav_top_align">
                        <i class="fa fa-pencil"></i>
                        Add Product
                    </h4>
                </div>
                <div class="col-sm-7 col-lg-6">
                    <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                        <li class="breadcrumb-item">
                            <a href="index1">
                                <i class="fa fa-home" data-pack="default" data-tags=""></i> Dashboard
                            </a>
                        </li>
                       
                        <li class="active breadcrumb-item"> Add Product</li>
                    </ol>
                </div>
            </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
              
                <!-- basic sign up form-->
                <div class="col-12 col-xl-6">
                    <div class="card m-t-35">
                        <div class="card-header bg-white">
                            Product
                        </div>
                        <div class="card-block">
                            <form class="form-horizontal" action="<?php echo e(URL::to('admin/addproduct')); ?>" method="post" >

                                <?php echo e(csrf_field()); ?>


                                 <input type="hidden" name="id" value="<?php echo e(isset($_GET['id'])?$_GET['id']:''); ?>">
                                 <input type="hidden" name="image" id="image" value="<?php echo e(isset($result['image'])?$result['image']:''); ?>">
                                <fieldset>
                                    <!-- Name input-->
                                    <div class="form-group row m-t-35">
                                        <div class="col-lg-3 text-lg-right">
                                            <label for="name" class="col-form-label">Name</label>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="input-group">
                                                            <span class="input-group-addon">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                <input type="text" name="product_name" id="product_name" class="form-control" placeholder="Product Name" required=""  value="<?php echo e(isset($result['product_name'])?$result['product_name']:''); ?>" >
                                            </div>
                                        </div>
                                    </div>
                                    <!-- first name-->
                                    <div class="form-group row">
                                        <div class="col-lg-3 text-lg-right">
                                            <label for="slug" class="col-form-label">Slug</label>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                <input type="text" name="product_slug" class="form-control" id="slug" placeholder="Slug" required value="<?php echo e(isset($result['product_slug'])?$result['product_slug']:''); ?>">
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <!-- last name-->
                                    <div class="form-group row">
                                        <div class="col-lg-3 text-lg-right">
                                            <label for="order" class="col-form-label">Sort Order</label>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="input-group">
                                                            <span class="input-group-addon">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                <input type="text" id="order" name="order" class="form-control" placeholder="Sort Order" value="<?php echo e(isset($result['order'])?$result['order']:0); ?>" required>
                                            </div>
                                        </div>
                                    </div>


                                    <!-- mail name-->
                                    <div class="form-group row">
                                         <div class="col-lg-3 text-lg-right">
                                            <label for="order" class="col-form-label">Category</label>
                                        </div>
                                   <div class="col-lg-8">
                                       
                                        <select class="form-control chzn-select" tabindex="2" name="category_id">
                                            <option disabled selected>Choose ..</option>

                                            <?php  if(count($cat) > 0) : ?>
                                                <?php $__currentLoopData = $cat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value['id']); ?>"><?php echo e($value['product_group']); ?></option>
                                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                             <?php  endif; ?>
                                           
                                        </select>
                                        <!--</div>-->
                                    </div>
                                </div>
                                    <!-- re password name-->
                                    <div class="form-group row">
                                        <div class="col-lg-3 text-lg-right">
                                            <label for="status" class="col-form-label">Status</label>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="input-group">
                                                            <span class="input-group-addon">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                <select class="form-control" id="status" name="status" required="" >
                                                    <option value="">Select</option>
                                                    <option value="1" <?php echo e((isset($result['status']) && $result['status'] == 1)?'selected':0); ?> >Active</option>
                                                    <option value="2"  <?php echo e((isset($result['status']) && $result['status'] == 2 )?'selected':0); ?>>Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                     <div class="form-group row">
                                        <div class="col-lg-3 text-lg-right">
                                            <label for="status" class="col-form-label">Price</label>
                                        </div>
                                        <div class="col-lg-8">
                                            <input name="price" type="text"   class="form-control" value="<?php echo e(isset($result['price'])?$result['price']:0); ?>" required  > 
                                    </div>
                                    </div>


                                     <div class="form-group row">
                                        <div class="col-lg-3 text-lg-right">
                                            <label for="status" class="col-form-label">Image Upload</label>
                                        </div>
                                        <div class="col-lg-8">
                                            <input id="input-21" type="file"  class="file-loading" > 
                                    </div>
                                    </div>

                                    

                                

                                    <!-- last name-->
                                    <div class="form-group row">
                                        <div class="col-lg-9 push-lg-3">
                                            <button class="btn btn-primary ">Save</button>
                                            
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>

                    </div>
                   
                </div>

                <?php if(isset($_GET['id'])): ?>
                <!-- Horizontal sign up form-->
                  <div class="col-12 col-xl-6">
                     <div class="card m-t-35">
                        <div class="card-header bg-white">
                            Seo Details
                        </div>
                        <div class="card-block">
                            <form class="form-horizontal" action="<?php echo e(URL::to('admin/addproduct')); ?>" method="post">
                                   <?php echo e(csrf_field()); ?>

                                      <input type="hidden" name="id" value="<?php echo e($_GET['id']); ?>">
                                <fieldset>

                                    <div class="form-group row m-t-35">
                                        <div class="col-lg-3 text-lg-right">
                                            <label for="name" class="col-form-label">Title</label>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="input-group">
                                                            <span class="input-group-addon">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                <input type="text" name="product_title" id="product_title" class="form-control" placeholder="Product Title" required=""  value="<?php echo e(isset($result['product_title'])?$result['product_title']:''); ?>" >
                                            </div>
                                        </div>
                                    </div>


                                    <!-- Name input-->
                                    <div class="form-group row m-t-35">
                                        <div class="col-lg-3 text-lg-right">
                                            <label for="name" class="col-form-label">Meta Descriptions</label>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="input-group">
                                                   <textarea id="autosize" class="form-control" cols="50" rows="5" name="product_meta_desc"><?php echo e(isset($result['product_meta_desc'])?$result['product_meta_desc']:''); ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- first name-->
                                    <div class="form-group row m-t-35">
                                        <div class="col-lg-3 text-lg-right">
                                            <label for="name" class="col-form-label">Keywords</label>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="input-group">
                                                   <textarea id="autosize" class="form-control" cols="50" rows="5" name="product_meta_keyword"><?php echo e(isset($result['product_meta_keyword'])?$result['product_meta_keyword']:''); ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <div class="form-group row">
                                        <div class="col-lg-9 push-lg-3">
                                            <button class="btn btn-primary ">Save</button>
                                            
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>

                    </div>

                    <div class="card m-t-35">
                        <div class="card-header bg-white">
                            Product Specifications
                             <div class="col-lg-3 push-lg-9">
                                <button class="btn btn-primary " id="addMorespcfitions">  <i class="fa fa-plus"></i> Add More</button>
                        </div>

                        </div>
                        <div class="card-block">
                            <form class="form-horizontal" action="<?php echo e(URL::to('admin/spefictions')); ?>" method="post">
                                   <?php echo e(csrf_field()); ?>

                                      <input type="hidden" name="product_id" value="<?php echo e($_GET['id']); ?>">
                                <fieldset class="addmore">
                                    
                                    <?php if(count($Spefictions) > 0 ): ?>
                                        <?php $__currentLoopData = $Spefictions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$spe): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="admBox">
                                            <div class="form-group row m-t-35">
                                        <div class="col-lg-5 text-lg-right">
                                            <input type="text" name="data[title][]"  class="form-control" placeholder="Title" value="<?php echo e($spe['title']); ?>" required=""  >
                                        </div>
                                        <div class="col-lg-6 text-lg-right">
                                            <input type="text" name="data[descr][]"  class="form-control" placeholder="Specifications" value="<?php echo e($spe['descr']); ?>" required=""  >
                                        </div>
                                         <div class="col-lg-1 ">
                                           <button class="btn btn-danger deletesda" type="button" onclick="remove(this)"><i class="fa fa-remove"></i> </button>
                                         </div>
                                        </div>
                                        </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    <?php endif ; ?>
                                 </fieldset>

                                    <div class="form-group row">
                                    <div class="col-lg-2 push-lg-10">
                                    <button class="btn btn-primary ">Save</button>
                                    </div>
                                    </div>

                            </form>
                        </div>

                    </div>

                </div>
                <?php endif; ?>
                <!-- end of horizontal signin layout-->
            </div>

            <?php if(isset($_GET['id'])): ?>
            <form class="form-horizontal" action="<?php echo e(URL::to('admin/addproduct')); ?>" method="post">
                                <?php echo e(csrf_field()); ?>

            <input type="hidden" name="id" value="<?php echo e($_GET['id']); ?>">
            
             <div class="row summer_note_display summer_note_btn">
                <div class="col-12">
                    <div class='card m-t-35'>
                        <div class='card-header bg-white '>
                            Description
                            <!-- tools box -->
                            <div class="float-right box-tools"></div>
                            <!-- /. tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class='card-block pad m-t-25'>
                           
                                    <textarea class="textarea form_editors_textarea_wysihtml"
                                              placeholder="Place some text here" name="product_desc"><?php echo e(isset($result['product_desc'])?$result['product_desc']:''); ?></textarea>
                         </div>

                        <div class="form-group row">
                        <div class="col-lg-2 push-lg-11">
                                 <button class="btn btn-primary ">Save</button>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
         </form>
         <?php endif; ?>




            <?php if(isset($_GET['id'])): ?>
            <form class="form-horizontal" action="<?php echo e(URL::to('admin/addproduct')); ?>" method="post">
                                <?php echo e(csrf_field()); ?>

            <input type="hidden" name="id" value="<?php echo e($_GET['id']); ?>">
            <div class="multipleImgeImput">
                <?php if(count($productimg) > 0 ) {
                    foreach ($productimg as $key => $value) { ?>
                        <input type="hidden" name="product_image[]" value="<?php echo e($value); ?>" >
                        <?php
                    }
                }  ?>

            </div>
             <div class="row summer_note_display summer_note_btn">
                <div class="col-12">
                    <div class='card m-t-35'>
                        <div class='card-header bg-white '>
                            Product Extra Image
                            <!-- tools box -->
                            <div class="float-right box-tools"></div>
                            <!-- /. tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class='card-block pad m-t-25'>
                              <input id="input-233" type="file"  class="file-loading" multiple >
                         </div>

                        <div class="form-group row">
                        <div class="col-lg-1 push-lg-11">
                                 <button class="btn btn-primary ">Save</button>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
         </form>
         <?php endif; ?>
        </div>
        <!-- /.inner -->
    </div>
    <!-- /.outer -->



<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer_scripts'); ?>
    <!--Plugin scripts-->
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/intl-tel-input/js/intlTelInput.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/sweetalert/js/sweetalert2.min.js')); ?>"></script>
    <!--End of Plugin scripts-->
    <!--Page level scripts-->
    <script type="text/javascript" src="<?php echo e(asset('assets/js/pages/form_layouts.js')); ?>"></script>
    <!-- end of page level js -->
     <!--Plugin scripts-->
 
     <script type="text/javascript" src="<?php echo e(asset('assets/vendors/summernote/js/summernote.min.js')); ?>"></script>

    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/fileinput/js/fileinput.min.js')); ?>"></script>
  
    <!-- end page level scripts -->
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/chosen/js/chosen.jquery.js')); ?>"></script>
    <script type="text/javascript">
         $(".chzn-select").chosen({allow_single_deselect: true});
         $('.chzn-select').val("<?php echo e(isset($result['category_id'])?$result['category_id']:''); ?>");
         $('.chzn-select').trigger('chosen:updated');

            $('.textarea').summernote({
        height:200
    });

            $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });

                $("#input-21").fileinput({
                theme: "fa",
                previewFileType: "image",
                browseClass: "btn btn-success",
                browseLabel: "Pick Image",
                removeClass: "btn btn-danger",
                removeLabel: "Delete",
                showPreview: true,
                allowedFileExtensions: ['jpg', 'png', 'gif'],
                maxFileCount: 1,
                uploadUrl: 'uploadImage',
                initialPreview: [
                '<?php echo e(asset($img)); ?>',
                ],
               
                initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
                initialPreviewFileType: 'image', // image is the default and can be overridden in config below
                }).on('fileuploaded', function(event,data,previewId, index) {
                   
                    var response = (data.response);
                   // alert(response.filename);
                    if(response != '') {
                     $("#image").val(response.filename);
                    $("#preveiewImage").attr('src',response.filename)
                    }else{
                    alert("upload failed!"+response.errmsg)
                    }
                });


                  $("#input-233").fileinput({
                theme: "fa",
                previewFileType: "image",
                browseClass: "btn btn-success",
                browseLabel: "Pick Image",
                removeClass: "btn btn-danger",
                removeLabel: "Delete",
                showPreview: true,
                allowedFileExtensions: ['jpg', 'png', 'gif'],
                maxFileCount: 4,
                uploadUrl: 'uploadImage',
                initialPreview: [
               <?php if(count($productimg) > 0 ) {
                    foreach ($productimg as $key => $value) { ?>
                        '<?php echo e(asset($value)); ?>',
                        <?php
                    }
                }  ?>
                ],
                initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
                initialPreviewFileType: 'image', 
                // image is the default and can be overridden in config below
                  append: true,
                }).on('fileuploaded', function(event,data,previewId, index) {
                   
                    var response = (data.response);
                   // alert(response.filename);

                    var inputHtml = '<input type="hidden" name="product_image[]"  value="'+response.filename+'"/>'
                    $('.multipleImgeImput').append(inputHtml);
                    /*if(response != '') {
                     $("#image").val(response.filename);
                    $("#preveiewImage").attr('src',response.filename)
                    }else{
                    alert("upload failed!"+response.errmsg)
                    }*/
                });






            

            $("#addMorespcfitions").on('click',function(){


                    var html = ' <div class="admBox"><div class="form-group row m-t-35"><div class="col-lg-5 text-lg-right"><input type="text" name="data[title][]"  class="form-control" placeholder="Title" required=""  ></div><div class="col-lg-6 text-lg-right"><input type="text" name="data[descr][]"  class="form-control" placeholder="Specifications" required=""  ></div> <div class="col-lg-1 text-lg-right"><button class="btn btn-danger deletesda" type="button" onclick="remove(this)"><i class="fa fa-remove"></i></button> </div></div>';
                    $(".addmore").after(html);

            })
          function remove(ev)
           {
             var  data =  $(ev).parents('.admBox').remove();
            console.log(data);
           }
          
         //  
          

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts/default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>