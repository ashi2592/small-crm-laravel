<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta Tags -->
    <meta charset="utf-8">
     <meta charset="UTF-8">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="themexriver">

    <!-- Page Title -->
    <title>  <?php $__env->startSection('title'); ?>
           Industry, Factory and Engineering HTML5 Template
        <?php echo $__env->yieldSection(); ?> </title>  
    <meta name="description" content="<?php echo $__env->yieldContent('description'); ?>">
    <meta name="keywords" content="<?php echo $__env->yieldContent('keywords'); ?>">

    <!-- Favicon and Touch Icons -->
    <link href="<?php echo e(asset('front/images/favicon/favicon.png')); ?> " rel="shortcut icon" type="image/png">
    <link href="<?php echo e(asset('front/images/favicon/apple-touch-icon.png')); ?>" rel="apple-touch-icon">
    <link href="<?php echo e(asset('front/images/favicon/apple-touch-icon-72x72.png')); ?>" rel="apple-touch-icon" sizes="72x72">
    <link href="<?php echo e(asset('front/images/favicon/apple-touch-icon-114x114.png')); ?>" rel="apple-touch-icon" sizes="114x114">
    <link href="<?php echo e(asset('front/images/favicon/apple-touch-icon-144x144.png')); ?> " rel="apple-touch-icon" sizes="144x144">

    <!-- Icon fonts -->
    <link type="text/css" href="<?php echo e(asset('front/css/font-awesome.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('front/css/flaticon.css')); ?>" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="<?php echo e(asset('front/css/bootstrap.min.css')); ?>" rel="stylesheet">

    <!-- Plugins for this template -->
    <link href="<?php echo e(asset('front/css/animate.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('front/css/owl.carousel.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('front/css/owl.theme.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('front/css/slick.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('front/css/slick-theme.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('front/css/owl.transitions.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('front/css/jquery.fancybox.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('front/css/bootstrap-select.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('front/css/magnific-popup.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('front/fancyzoom/css/fancyzoom.css')); ?>" rel="stylesheet">

  
   
   <!-- Custom styles for this template -->
     <link href="<?php echo e(asset('front/css/style.css')); ?>" rel="stylesheet">
     <?php echo $__env->yieldContent('header_styles'); ?>

</head>

<body class="home-style-3">

    <!-- start page-wrapper -->
    <div class="page-wrapper">

        <!-- start preloader -->
        <div class="preloader">
            <div class="preloader-inner">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <!-- end preloader -->
        <?php echo $__env->make('front.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        
        <?php echo $__env->yieldContent('content'); ?>
        
      
        <?php echo $__env->make('front.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->make('front.enquiry', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    </div>
    <!-- end of page-wrapper -->


    <!-- All JavaScript files
    ================================================== -->
    <script src="<?php echo e(asset('front/js/jquery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('front/js/bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('front/fancyzoom/js/jquery.fancyzoom.js')); ?>"></script>

    <!-- Plugins for this template -->
    <script src="<?php echo e(asset('front/js/jquery-plugin-collection.js')); ?>"></script>

    <!-- Custom script for this template -->
    <script src="<?php echo e(asset('front/js/script.js')); ?>"></script>
    <script type="text/javascript">
    $(document).ready(function() {
    $("a.fancy").fancyZoom();
});
</script>

    <?php echo $__env->yieldContent('footer_script'); ?>

</body>

</html>
