<?php $__env->startSection('title'); ?>
    Addtional Service
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header_styles'); ?>
    <!--plugin styles-->
    <link rel="stylesheet" href="<?php echo e(asset('assets/vendors/intl-tel-input/css/intlTelInput.css')); ?>">
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')); ?>" />
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/sweetalert/css/sweetalert2.min.css')); ?>" />
    <!--End of plugin styles-->
    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/css/pages/sweet_alert.css')); ?>" />
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/css/pages/form_layouts.css')); ?>" />
    <!-- end of page level styles -->

    <!-- global styles-->
    <link type="text/css" rel="stylesheet" media="screen" href="<?php echo e(asset('assets/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css')); ?>"/>
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/summernote/css/summernote.css')); ?>"/>
    <!-- end of global styles-->
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/css/pages/form_elements.css')); ?>"/>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <header class="head">
        <div class="main-bar">
            <div class="row no-gutters">
                <div class="col-sm-5 col-lg-6 skin_txt">
                    <h4 class="nav_top_align">
                        <i class="fa fa-pencil"></i>
                        Add Addtional Service
                    </h4>
                </div>
                <div class="col-sm-7 col-lg-6">
                    <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                        <li class="breadcrumb-item">
                            <a href="index1">
                                <i class="fa fa-home" data-pack="default" data-tags=""></i> Dashboard
                            </a>
                        </li>
                       
                        <li class="active breadcrumb-item"> Add Addtional Service</li>
                    </ol>
                </div>
            </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
              
                <!-- basic sign up form-->
                <div class="col-12 col-xl-6">
                    <div class="card m-t-35">
                        <div class="card-header bg-white">
                            Add Addtional Service
                        </div>
                        <div class="card-block">
                            <form class="form-horizontal" action="<?php echo e(URL::to('admin/addadditionalprice')); ?>" method="post">
                                <?php echo e(csrf_field()); ?>

                                <input type="hidden" name="id" id="name" class="form-control" placeholder="Addtional Service Name" required=""  value="<?php echo e(isset($result['id'])?$result['id']:''); ?>" >

                                <fieldset>
                                    <!-- Name input-->
                                    <div class="form-group row m-t-35">
                                        <div class="col-lg-3 text-lg-right">
                                            <label for="name" class="col-form-label">Addtional Service </label>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="input-group">
                                                            <span class="input-group-addon">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                <input type="text" name="additionalservice" id="name" class="form-control" placeholder="Addtional Service Name" required=""  value="<?php echo e(isset($result['additionalservice'])?$result['additionalservice']:''); ?>" >
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Name input-->
                                    <div class="form-group row ">
                                        <div class="col-lg-3 text-lg-right">
                                            <label for="name" class="col-form-label">key</label>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="input-group">
                                                            <span class="input-group-addon">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                <input type="text" name="additionalkey" id="name" class="form-control" placeholder="Addtional Service key" required=""  value="<?php echo e(isset($result['additionalkey'])?$result['additionalkey']:''); ?>" >
                                            </div>
                                        </div>
                                    </div>
                                               

                                               <!-- Name input-->
                                    <div class="form-group row ">
                                        <div class="col-lg-3 text-lg-right">
                                            <label for="name" class="col-form-label">Price</label>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="input-group">
                                                            <span class="input-group-addon">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                <input type="text" name="price" id="name" class="form-control" placeholder="Addtional Service Price" required=""  value="<?php echo e(isset($result['price'])?$result['price']:''); ?>" >
                                            </div>
                                        </div>
                                    </div>
                                                                  
                                   
                                    <!-- re password name-->
                                    <div class="form-group row">
                                        <div class="col-lg-3 text-lg-right">
                                            <label for="status" class="col-form-label">Status</label>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="input-group">
                                                            <span class="input-group-addon">
                                                            <i class="fa fa-check"></i>
                                                        </span>
                                                <select class="form-control" id="status" name="status" required="" >
                                                    <option value="">Select</option>
                                                    <option value="1" <?php echo e((isset($result['status']) && $result['status'] == 1)?'selected':0); ?> >Active</option>
                                                    <option value="2"  <?php echo e((isset($result['status']) && $result['status'] == 2 )?'selected':0); ?>>Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- last name-->

                                   

                                    <div class="form-group row">
                                        <div class="col-lg-9 push-lg-3">
                                            <button class="btn btn-primary ">Save</button>
                                            
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>

                    </div>
                   
                </div>

              
                <!-- end of horizontal signin layout-->
            </div>

         
        </div>
        <!-- /.inner -->
    </div>
    <!-- /.outer -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer_scripts'); ?>
    <!--Plugin scripts-->
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/intl-tel-input/js/intlTelInput.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/sweetalert/js/sweetalert2.min.js')); ?>"></script>
    <!--End of Plugin scripts-->
    <!--Page level scripts-->
    <script type="text/javascript" src="<?php echo e(asset('assets/js/pages/form_layouts.js')); ?>"></script>
    <!-- end of page level js -->
     <!--Plugin scripts-->
 
    <script type="text/javascript" src="<?php echo e(asset('assets/vendors/bootstrap3-wysihtml5-bower/js/bootstrap3-wysihtml5.all.min.js')); ?>"></script>
    <!--Page level scripts-->
    <script type="text/javascript" src="<?php echo e(asset('assets/js/pages/form_editors.js')); ?>"></script>
    <!-- end page level scripts -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts/default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>