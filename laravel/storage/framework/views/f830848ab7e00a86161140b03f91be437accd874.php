 <!-- start of services-serction-s3 --> 
        <section class="services-serction-s3 section-padding">
            <div class="container">
                <div class="row section-title-s6">
                    <div class="col col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1">
                        <h2><span>Our</span> Products</h2>
                        <h3>AGARBATTI AND PAPER PLATE MAKING MACHINES</h3>
                        <p>We are the leading Manufacturer of Agarbatti Making Machine, Foil Container Making Machine, Plate Making Machine, Vacuum Forming Machine, etc. Our presented products are extremely admired for their longer life, sturdiness and durability and good quality. Our business practices are purely ethical and we encourage a communicative approach towards all our dealings.</p>
                    </div>
                </div> <!-- end section-title -->
            <?php $products = commonhelper::getProductsbyids([1,2,3,4,5,6]); /*echo "<pre>"; print_r($products);*/ ?>
                <div class="row services-s2-grids">

                    <?php if(count($products)): ?>
                        <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        
                            <div class="col col-lg-4 col-xs-6">
                            <div class="grid">
                                 <a class="fancy" href="<?php echo e(asset($product['image'])); ?>" data-fancybox-group="gallery" title="Image Title ">
                                <div class="img-holder">
                                    <img src="<?php echo e(asset($product['image'])); ?>" alt class="img img-responsive">
                                </div>
                                 </a>
                                <div class="details">
                                    <h3><a href="<?php echo e(url('/product')); ?>/<?php echo e($product['product_slug']); ?>"><?php echo e($product['product_name']); ?></a></h3>
                                    <p><?php echo e(commonhelper::strip_tag(htmlspecialchars_decode($product['product_desc']),32)); ?></p>
                                    <a href="<?php echo e(url('/product')); ?>/<?php echo e($product['product_slug']); ?>" class="read-more"><i class="fa fa-plus-square-o" aria-hidden="true"></i> Read More</a>
                                </div>
                            </div>
                        </div>
                        

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    <?php endif; ?>
                   
                </div> <!-- end row -->
            </div> <!-- end container -->
        </section>
        <!-- end of services-serction-s3 -->