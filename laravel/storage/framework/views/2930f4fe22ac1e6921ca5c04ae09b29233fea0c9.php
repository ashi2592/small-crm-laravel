  <!-- start of hero -->   
        <section class="hero hero-slider-wrapper hero-slider-s2">
            <div class="hero-slider">
                <div class="slide">
                    <img src="<?php echo e(asset('front/images/slider/slide-3.jpg')); ?>" alt class="slider-bg">
                    
                </div>

                <div class="slide">
                    <img src="<?php echo e(asset('front/images/slider/slide-1.jpg')); ?>" alt class="slider-bg">
                    
                </div>

                <div class="slide">
                    <img src="<?php echo e(asset('front/images/slider/slide-2.jpg')); ?>" alt class="slider-bg">
                    
                </div>
            </div>
        </section>
        <!-- end of hero slider -->