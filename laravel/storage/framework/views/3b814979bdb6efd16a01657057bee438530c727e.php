<?php $__env->startSection('title'); ?>
##parent-placeholder-3c6de1b7dd91465d437ef415f94f36afc1fbc8a8##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('description','keyword'); ?>
<?php $__env->startSection('keywords',''); ?>
<?php $__env->startSection('header_styles'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('front.slider', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>;

 <?php echo $__env->make('front.services', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>;
   <?php echo $__env->make('front.testimonial', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>;
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer_script'); ?>
<script type="text/javascript">
    $(document).ready(function() {
    $("a.fancy").fancyZoom();
});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts/frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>