<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pricing extends Model
{
	protected $table = 'pricing';
  	protected $fillable = [
         'visa_id','country_id','processing_id','amount','status','product_name','productinfo'
    ];
}
