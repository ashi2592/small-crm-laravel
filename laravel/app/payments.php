<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class payments extends Model
{
	protected $table = 'payments';
  	protected $fillable = [
         'firstname', 'email', 'phone', 'amount', 'productinfo', 'website_id','payment_status','payment_date','txnid'
    ];
}
