<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class country extends Model
{
	protected $table = 'country';
  	protected $fillable = [
         'country_name', 'code','flag_image','status'
    ];
}
