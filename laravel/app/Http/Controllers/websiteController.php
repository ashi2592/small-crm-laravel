<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\website;
class websiteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


      /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $result =  website::get()->toArray();
        return view('website.index')->with(compact('result'));
    }



   public function add(Request $request)
    {

        if ($request->isMethod('post')) 
        {
            $input = $request->all();
       		unset($input['_token']);
            $data = $input;
       		$id = isset($input['id'])?$input['id']:'';
			$Save = website::firstOrNew(['id'=>$id]);
			$Save->fill($data);
			$Save->save();
					return redirect()->action(
					'websiteController@index', ['id' => $Save->id]
					);
        }

          if($request->isMethod('get') && $request->id)
          {

            $id =  $request->id;
            $result =  website::where('id',$id)->get()->first();
            if(count($result)> 0)
            {
            return view('website.add')->with(compact('result'));                
            }
          }

        return view('website.add');
    }


    public function deletedata(Request $request)
    {
        if($request->isMethod('get') && $request->id)
          {
           $id =  $request->id;
           }
        $flight = website::where('id', '=', $id)->delete();
            return redirect()->action('websiteController@index');
    }
    
}

?>