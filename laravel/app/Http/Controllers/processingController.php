<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\processing;

class processingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


      /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      /* echo config('constant.payment_gateway.paypal.id');
       exit;*/
         $result =  processing::get()->toArray();
        return view('processing.index')->with(compact('result'));
    }



   public function add(Request $request)
    {

        if ($request->isMethod('post')) 
        {
            $input = $request->all();
       		unset($input['_token']);
            $data = $input;
       		$id = isset($input['id'])?$input['id']:'';
			$Save = processing::firstOrNew(['id'=>$id]);
			$Save->fill($data);
			$Save->save();
					return redirect()->action(
					'processingController@index', ['id' => $Save->id]
					);
        }

          if($request->isMethod('get') && $request->id)
          {

            $id =  $request->id;
            $result =  processing::where('id',$id)->get()->first();
            if(count($result)> 0)
            {
            return view('processing.add')->with(compact('result'));                
            }
          }

        return view('processing.add');
    }


    public function deletedata(Request $request)
    {
        if($request->isMethod('get') && $request->id)
          {
           $id =  $request->id;
           }
        $flight = processing::where('id', '=', $id)->delete();
            return redirect()->action('processingController@index');
    }
    
}

?>