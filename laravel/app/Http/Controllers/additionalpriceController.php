<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\additionalprice;
class additionalpriceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


      /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $result =  additionalprice::get()->toArray();
        return view('additionalprice.index')->with(compact('result'));
    }



   public function add(Request $request)
    {

        if ($request->isMethod('post')) 
        {
            $input = $request->all();
       		unset($input['_token']);
            $data = $input;
       		$id = isset($input['id'])?$input['id']:'';
			$Save = additionalprice::firstOrNew(['id'=>$id]);
			$Save->fill($data);
			$Save->save();
					return redirect()->action('additionalpriceController@index');
        }

          if($request->isMethod('get') && $request->id)
          {

            $id =  $request->id;
            $result =  additionalprice::where('id',$id)->get()->first();
            if(count($result)> 0)
            {
            return view('additionalprice.add')->with(compact('result'));                
            }
          }

        return view('additionalprice.add');
    }


    public function deletedata(Request $request)
    {
        if($request->isMethod('get') && $request->id)
          {
           $id =  $request->id;
           }
        $flight = additionalprice::where('id', '=', $id)->delete();
            return redirect()->action('additionalpriceController@index');
    }
    
}

?>