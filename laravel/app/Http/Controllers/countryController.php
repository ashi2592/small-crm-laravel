<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\country;
class countryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


      /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $result =  country::get()->toArray();
        return view('country.index')->with(compact('result'));
    }



   public function add(Request $request)
    {

        if ($request->isMethod('post')) 
        {
            $input = $request->all();
       		unset($input['_token']);
            $data = $input;
       		$id = isset($input['id'])?$input['id']:'';
			$Save = country::firstOrNew(['id'=>$id]);
			$Save->fill($data);
			$Save->save();
					return redirect()->action('countryController@index');
        }

          if($request->isMethod('get') && $request->id)
          {

            $id =  $request->id;
            $result =  country::where('id',$id)->get()->first();
            if(count($result)> 0)
            {
            return view('country.add')->with(compact('result'));                
            }
          }

        return view('country.add');
    }


    public function deletedata(Request $request)
    {
        if($request->isMethod('get') && $request->id)
          {
           $id =  $request->id;
           }
        $flight = country::where('id', '=', $id)->delete();
            return redirect()->action('countryController@index');
    }
    
}

?>