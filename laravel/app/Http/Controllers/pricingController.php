<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\pricing;
use App\country;
use App\Visa;
use App\processing;

class pricingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


      /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      /* echo config('constant.payment_gateway.paypal.id');
       exit;*/
         $result =  pricing::get()->toArray();
        return view('pricing.index')->with(compact('result'));
    }



   public function add(Request $request)
    {

        if ($request->isMethod('post')) 
        {
            $input = $request->all();
       		unset($input['_token']);
            $data = $input;
            $id= $input['id'];

          $Save = pricing::firstOrNew(['id'=>$id]);
          $Save->fill($data);
          $Save->save();
					return redirect()->action('pricingController@add', ['id' => $Save->id]);
        }

        $visatypes = Visa::select('id','visa_name')->where('status',1)->get()->toArray();
        $country = country::select('id','country_name')->where('status',1)->get()->toArray();
        $processing = processing::select('id','processing')->where('status',1)->get()->toArray();
         $results =  pricing::get()->toArray();
     
      if($request->isMethod('get') && $request->id)
      {
      $priceinfo =  pricing::where('id',$request->id)->first()->toArray();
      return view('pricing.add')->with(compact('visatypes','country','processing','results','priceinfo'));
      }
      else{
      return view('pricing.add')->with(compact('visatypes','country','processing','results'));

      }        



      }


    public function deletedata(Request $request)
    {
        if($request->isMethod('get') && $request->id)
          {
           $id =  $request->id;
           }
        $flight = pricing::where('id', '=', $id)->delete();
            return redirect()->action('pricingController@add');
    }
    
}

?>