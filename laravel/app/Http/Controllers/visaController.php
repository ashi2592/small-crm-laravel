<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Visa;
class visaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


      /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $result =  Visa::get()->toArray();
        return view('visa.index')->with(compact('result'));
    }



   public function add(Request $request)
    {

        if ($request->isMethod('post')) 
        {
            $input = $request->all();
       		unset($input['_token']);
            $data = $input;
       		$id = isset($input['id'])?$input['id']:'';
			$Save = Visa::firstOrNew(['id'=>$id]);
			$Save->fill($data);
			$Save->save();
					return redirect()->action(
					'visaController@index', ['id' => $Save->id]
					);
        }

          if($request->isMethod('get') && $request->id)
          {

            $id =  $request->id;
            $result =  Visa::where('id',$id)->get()->first();
            if(count($result)> 0)
            {
            return view('visa.add')->with(compact('result'));                
            }
          }

        return view('visa.add');
    }


    public function deletedata(Request $request)
    {
        if($request->isMethod('get') && $request->id)
          {
           $id =  $request->id;
           }
        $flight = Visa::where('id', '=', $id)->delete();
            return redirect()->action('visaController@index');
    }
    
}

?>