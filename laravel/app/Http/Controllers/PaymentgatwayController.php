<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\payments;
use App\website;
use App\pricing;
use App\additionalprice;
class PaymentgatwayController extends Controller
{
      /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $result =  payments::get()->toArray();
       return view('payment.index')->with(compact('result'));
    }


    
   public function add($website_id,Request $request)
    {
     
      $website_data =  website::where('id',$website_id)->first();
      $SALT           =  isset($website_data->salt_key)?$website_data->salt_key:'';
      $MERCHANT_KEY   = isset($website_data->marchent_key)?$website_data->marchent_key:'';
       if(isset($website_data->payment_gateway_mode) &&  $website_data->payment_gateway_mode == 1)
      {
         $PAYU_BASE_URL = isset($website_data->payment_gateway_url_live)?$website_data->payment_gateway_url_live:'';
      } else{
         $PAYU_BASE_URL = isset($website_data->payment_gateway_url_test)?$website_data->payment_gateway_url_test:'';
      }
     
      

      if($request->isMethod('post'))
      {
         $inputs = $request->all();
          $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
          $hashVarsSeq = explode('|', $hashSequence);
          $hash_string = '';  
          foreach($hashVarsSeq as $hash_var) {
          $hash_string .= isset($inputs[$hash_var]) ? $inputs[$hash_var] : '';
          $hash_string .= '|';
          }
          $hash_string .= $SALT; 
          $hash = strtolower(hash('sha512', $hash_string));
          $action = $PAYU_BASE_URL . '/_payment';
          $txnid = $inputs['txnid'];
          $data = $inputs;
          $furl = $inputs['furl'];
          $surl = $inputs['surl'];
          $curl = $inputs['curl'];
      }
      else{
        $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
        $inputs = $request->query();
        $data = [];
        $data['txnid'] = isset($txnid)?$txnid:'';
        $data['amount'] = isset($inputs['amount'])?$inputs['amount']:'';
        $data['productinfo'] = isset($inputs['productinfo'])?$inputs['productinfo']:'';
        $data['firstname'] = isset($inputs['firstname'])?$inputs['firstname']:'';
        $data['email'] = isset($inputs['email'])?$inputs['email']:'';
        $data['phone'] = isset($inputs['phone'])?$inputs['phone']:'';
        $data['website_id'] = isset($website_id)?$website_id:'';
        $data['payment_date'] = date('Y-d-m');
        $save = payments::create($data);
        $id = $save->id;
        $furl = isset($website_data->payment_gateway_furl)?$website_data->payment_gateway_furl.'/'.$txnid:'';
        $surl = isset($website_data->payment_gateway_surl)?$website_data->payment_gateway_surl.'/'.$txnid:'';
        $curl = isset($website_data->payment_gateway_furl)?$website_data->payment_gateway_furl.'/'.$txnid:'';
        $action = url('/payments/'.$website_id);
        $hash = '';
        
      }
     
      return view('payment.api')->with(compact('txnid','PAYU_BASE_URL','MERCHANT_KEY','SALT','hash','data','surl','furl','action','curl'));  
    }


 




public function surl($id,Request $request)
    {

      $inputs =  $request->all();
      if($inputs['status'] == 'failure')
      {
         payments::where('txnid',$id)->update(['payment_status'=>2]);
          echo 'failure';
      }else{
          payments::where('txnid',$id)->update(['payment_status'=>1]);
          echo 'Success';
      }
    }

public function furl($id,Request $request)
    {
    
      echo "error";
      exit;
    }

  public function newPaymentdetails($email=null,$phone=null,$firstname=null,$website=null,$price=null,$addtionalPayment=null,$payemnetgateway=1,$currency=1)
  {


    $otherdata = [];
    $pi ='';
    $totalprice = 0;

    if($email != null && $phone !=null && $firstname !=null && $website != null && $price !=null)
    { 

      $otherdata['email']     = $email;
      $otherdata['phone']     = $phone;
      $otherdata['firstname'] = $firstname;


         $pricing = pricing::where('id',$price)->where('status',1)->first();
          $pi = isset($pricing->product_name)?'Product '.$pricing->product_name:''; 
          $pi .= isset($pricing->productinfo)?' Productinfo '.$pricing->productinfo:''; 

          $totalprice +=  $pricing->amount;
         if($addtionalPayment != null)
         {
          $apa = explode('_', $addtionalPayment);
          $a_p_d = additionalprice::select('price','additionalservice','additionalkey')->whereIn('id',$apa)->get();
          if(count($a_p_d))
          {

            foreach ($a_p_d as $key => $apd) {
              $totalprice +=  $apd['price'];
            }
          }
         }
          $otherdata['amount'] = $totalprice;
          $otherdata['productinfo'] = $pi;
          


          $website_data =  website::where('id',$website)->first();
       $SALT           =  isset($website_data->salt_key)?$website_data->salt_key:'';
     $MERCHANT_KEY   = isset($website_data->marchent_key)?$website_data->marchent_key:'';
       if(isset($website_data->payment_gateway_mode) &&  $website_data->payment_gateway_mode == 1)
      {
         $PAYU_BASE_URL = isset($website_data->payment_gateway_url_live)?$website_data->payment_gateway_url_live:'';
      } else{
        $PAYU_BASE_URL = isset($website_data->payment_gateway_url_test)?$website_data->payment_gateway_url_test:'';
      }
     
      $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
      $data = [];
      $data['txnid'] = isset($txnid)?$txnid:'';
      $data['amount'] = isset($totalprice)?$totalprice:'';
      $data['productinfo'] = isset($pi)?$pi:'';
      $data['firstname'] = isset($firstname)?$firstname:'';
      $data['email'] = isset($email)?$email:'';
      $data['phone'] = isset($phone)?$phone:'';
      $data['website_id'] = isset($website)?$website:'';
      $data['payment_date'] = date('Y-d-m');
      $save = payments::create($data);
      $id = $save->id;
      $furl = isset($website_data->payment_gateway_furl)?$website_data->payment_gateway_furl.'/'.$txnid:'';
      $surl = isset($website_data->payment_gateway_surl)?$website_data->payment_gateway_surl.'/'.$txnid:'';
      $curl = isset($website_data->payment_gateway_furl)?$website_data->payment_gateway_furl.'/'.$txnid:'';
      $action = url('/payments/'.$website);
      $hash = '';
      return view('payment.api2')->with(compact('txnid','PAYU_BASE_URL','MERCHANT_KEY','SALT','hash','data','surl','furl','action','curl')); 



    }
    else{
      echo"Paramenters are not Availabel";

    }

  }



}

?>