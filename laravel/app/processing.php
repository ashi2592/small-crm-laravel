<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class processing extends Model
{
	protected $table = 'processing';
  	protected $fillable = [
         'processing', 'status'
    ];
}
