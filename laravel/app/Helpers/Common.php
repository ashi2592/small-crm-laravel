<?php

namespace App\Helpers;

use App\Category;
use App\Product;
use App\Leads;
use App\Gallery;
use App\Page;
use App\Productspec;


class Common {

    public static function getGSTOtp() {
        return "Hello, Ashish";
    }

    public static function getproductCount()
    {
    	$count = 	Product::count();
    	return $count;
    }

    public static function getcategorytCount()
    {
    	$count = 	Category::count();
    	return $count;
    }

    public static function getLeadCount()
    {
    	$count = 	Leads::count();
    	return $count;
    }

    public static function getGalleryCount()
    {
    	$count = 	Gallery::count();
    	return $count;
    }

	public static function getProducts($limit=10,$fields=['*'])
	{
		$result = 	Product::select($fields)->limit($limit)->get()->toArray();
		return $result;
	}

    public static function getCategorys($fields=['*'])
    {
        $result =   Category::select($fields)->get()->toArray();
        return $result;
    }


     public static function getCategoryIdByname($slug)
    {
        $result =   Category::where('product_group_slug',$slug)->first()->toArray();
        return $result;
    }

     public static function getProudctByname($slug)
    {
        $result =   Product::where('product_slug',$slug)->first()->toArray();
        return $result;
    }



    public static function getProuctBycategoryId($id='')
    {
        $result =   Product::where('category_id',$id)->orderBy('order', 'asc')->get()->toArray();
        return $result;
    }


	public static function getProductsbyid($id=1)
	{
		$result = 	Product::where('id',$id)->get()->toArray();
		return $result;
	}
	public static function getProductsbyids($ids=[])
	{
		$result = 	Product::whereIn('id',$ids)->orderBy('order', 'asc')->get()->toArray();
		return $result;
	}


    public static function getPage($id=0)
    {
        $result =   Page::where('id',$id)->first()->toArray();
        return $result;
    }
    
    public static function strip_tag($string,$length=25)
    {
    if(strlen($string) > $length) {
    // truncate string
    $stringCut = substr($string, 0, $length);

    // make sure it ends in a word so assassinate doesn't become ass...
    $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
    }
    return $string;
    }

}
