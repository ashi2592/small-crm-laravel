<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class website extends Model
{
	protected $table = 'website';
	protected $fillable = [
	     'webiste_name', 'payment_gateway_url_live', 'payment_gateway_url_test', 'payment_gateway_mode', 'marchent_key', 'salt_key','status','payment_gateway_furl','payment_gateway_surl'
	];
}
