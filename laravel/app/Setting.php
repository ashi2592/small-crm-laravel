<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
	 protected $table = 'setting';

   protected $fillable = [
       'toogle', 'contact_email', 'contact_phone', 'contact_address', 'created_at', 'updated_at'
    ];

}
