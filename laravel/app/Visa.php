<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visa extends Model
{
	protected $table = 'visa';
  	protected $fillable = [
         'visa_name', 'status'
    ];
}
