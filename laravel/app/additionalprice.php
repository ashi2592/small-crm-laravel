<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class additionalprice extends Model
{
	protected $table = 'additionalprice';
  	protected $fillable = [
         'additionalservice','additionalkey','price','status'
    ];
}
