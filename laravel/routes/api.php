<?php

use Illuminate\Http\Request;
use App\Leads;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/addLeads',function(Request $request){
	$inputs = $request->all();
	$insertData = [];
	$insertData['name'] = isset($inputs['name'])?$inputs['name']:'';
	$insertData['email'] = isset($inputs['email'])?$inputs['email']:'';
	$insertData['phone'] = isset($inputs['phone'])?$inputs['phone']:'';
	$insertData['query'] = isset($inputs['query'])?$inputs['query']:'';
	$insertData['product_id'] = isset($inputs['product_id'])?$inputs['product_id']:0;
	Leads::create($insertData);
	echo 1;
});