<?php

Route::get('/', 'websiteController@index');
Route::get('/payments/{website_id}', 'PaymentgatwayController@add');
Route::post('/payments/{website_id}', 'PaymentgatwayController@add');
Route::any('/payments-surl/{id}', 'PaymentgatwayController@surl');
Route::any('/payments-furl/{id}', 'PaymentgatwayController@furl');

Auth::routes();
Route::get('/admin', 'websiteController@index');
Route::get('/admin/website', 'websiteController@index');
Route::get('/admin/addwebsite', 'websiteController@add');
Route::post('/admin/addwebsite', 'websiteController@add');
Route::get('/admin/deletewebsite', 'websiteController@deletedata');
Route::get('/admin/payments', 'PaymentgatwayController@index');




Route::get('/admin/country','countryController@index');
Route::get('/admin/addCountry', 'countryController@add');
Route::post('/admin/addCountry', 'countryController@add');
Route::get('/admin/deleteCountry', 'countryController@deletedata');



Route::get('/admin/visa','visaController@index');
Route::get('/admin/addvisa', 'visaController@add');
Route::post('/admin/addvisa', 'visaController@add');
Route::get('/admin/deletevisa', 'visaController@deletedata');



Route::get('/admin/processing','processingController@index');
Route::get('/admin/addprocessing', 'processingController@add');
Route::post('/admin/addprocessing', 'processingController@add');
Route::get('/admin/deleteprocessing', 'processingController@deletedata');



Route::get('/admin/additionalprice','additionalpriceController@index');
Route::get('/admin/addadditionalprice', 'additionalpriceController@add');
Route::post('/admin/addadditionalprice', 'additionalpriceController@add');
Route::get('/admin/deleteadditionalprice', 'additionalpriceController@deletedata');

Route::get('/admin/pricing','pricingController@add');
Route::get('/admin/addpricing', 'pricingController@add');
Route::post('/admin/addpricing', 'pricingController@add');
Route::get('/admin/deletepricing', 'pricingController@deletedata');

Route::get('/newpayments/{email}/{phone}/{name}/{website_id}/{price}/{additionalprice?}/', 'PaymentgatwayController@newPaymentdetails');



