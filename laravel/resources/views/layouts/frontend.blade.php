<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta Tags -->
    <meta charset="utf-8">
     <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="themexriver">

    <!-- Page Title -->
    <title>  @section('title')
           Industry, Factory and Engineering HTML5 Template
        @show </title>  
    <meta name="description" content="@yield('description')">
    <meta name="keywords" content="@yield('keywords')">

    <!-- Favicon and Touch Icons -->
    <link href="{{asset('front/images/favicon/favicon.png')}} " rel="shortcut icon" type="image/png">
    <link href="{{asset('front/images/favicon/apple-touch-icon.png')}}" rel="apple-touch-icon">
    <link href="{{asset('front/images/favicon/apple-touch-icon-72x72.png')}}" rel="apple-touch-icon" sizes="72x72">
    <link href="{{asset('front/images/favicon/apple-touch-icon-114x114.png')}}" rel="apple-touch-icon" sizes="114x114">
    <link href="{{asset('front/images/favicon/apple-touch-icon-144x144.png')}} " rel="apple-touch-icon" sizes="144x144">

    <!-- Icon fonts -->
    <link type="text/css" href="{{asset('front/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/flaticon.css')}}" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="{{asset('front/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Plugins for this template -->
    <link href="{{asset('front/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/owl.carousel.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/owl.theme.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/slick.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/slick-theme.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/owl.transitions.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/jquery.fancybox.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/bootstrap-select.min.css')}}" rel="stylesheet">
    <link href="{{asset('front/css/magnific-popup.css')}}" rel="stylesheet">
    <link href="{{asset('front/fancyzoom/css/fancyzoom.css')}}" rel="stylesheet">

  
   
   <!-- Custom styles for this template -->
     <link href="{{asset('front/css/style.css')}}" rel="stylesheet">
     @yield('header_styles')

</head>

<body class="home-style-3">

    <!-- start page-wrapper -->
    <div class="page-wrapper">

        <!-- start preloader -->
        <div class="preloader">
            <div class="preloader-inner">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <!-- end preloader -->
        @include('front.header')
        
        @yield('content')
        
      
        @include('front.footer')
        @include('front.enquiry')

    </div>
    <!-- end of page-wrapper -->


    <!-- All JavaScript files
    ================================================== -->
    <script src="{{asset('front/js/jquery.min.js')}}"></script>
    <script src="{{asset('front/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('front/fancyzoom/js/jquery.fancyzoom.js')}}"></script>

    <!-- Plugins for this template -->
    <script src="{{asset('front/js/jquery-plugin-collection.js')}}"></script>

    <!-- Custom script for this template -->
    <script src="{{asset('front/js/script.js')}}"></script>
    <script type="text/javascript">
    $(document).ready(function() {
    $("a.fancy").fancyZoom();
});
</script>

    @yield('footer_script')

</body>

</html>
