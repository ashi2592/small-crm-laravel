@extends('layouts/default')

{{-- Page title --}}
@section('title')
    Payments
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
    <!--Plugin styles-->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/select2/css/select2.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/dataTables.bootstrap.css')}}"/>
    <!--End of plugin styles-->
    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tables.css')}}"/>
    <!-- end of page level styles -->

@stop
@section('content')
    <header class="head">
        <div class="main-bar">
            <div class="row no-gutters">
                <div class="col-lg-6 col-sm-4">
                    <h4 class="nav_top_align">
                        <i class="fa fa-user"></i>
                        Payments
                    </h4>
                </div>
                <div class="col-lg-6 col-sm-8 col-12">
                    <ol class="breadcrumb float-right  nav_breadcrumb_top_align">
                        <li class="breadcrumb-item">
                            <a href="index1">
                                <i class="fa fa-home" data-pack="default" data-tags=""></i> Dashboard
                            </a>
                        </li>
                        <li class="active breadcrumb-item">Payments</li>
                    </ol>
                </div>
            </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
            <div class="card">
                <div class="card-header bg-white">
                    Payments
                </div>
                <div class="card-block m-t-35" id="user_body">
                    <div class="table-toolbar">
                       
                        <div class="btn-group float-right users_grid_tools">
                            <div class="tools"></div>
                        </div>
                    </div>
                    <div>
                        <div>
                            <table class="table  table-striped table-bordered table-hover dataTable no-footer"
                                   id="editable_table" role="grid">
                                <thead>
                                <tr role="row">
                                    <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">PId</th>
                                    <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1"> Name</th>
                                    <th class="sorting wid-25" tabindex="0" rowspan="1" colspan="1">amount </th>
                                    <th class="sorting wid-10" tabindex="0" rowspan="1" colspan="1">productinfo</th>
                                    <th class="sorting wid-15" tabindex="0" rowspan="1" colspan="1">website_id</th>
                                    <th class="sorting wid-10" tabindex="0" rowspan="1" colspan="1">Status</th>
                                </tr>
                                </thead>
                                <tbody>         
                                @if(count($result))                      
                                    <?php foreach ($result as $key => $value) :?>
                                <tr role="row" class="even">
                                    <td class="sorting_1">{{$value['id']}}</td>
                                    <td>{{$value['firstname']}}</td>
                                    <td>{{$value['amount']}}</td>
                                    <td>{{$value['productinfo']}}</td>
                                    <td>{{$value['website_id']}}</td>

                                    <?php
                                    if($value['payment_status'] == 0)
                                    {
                                    $pay = 'pending';
                                    }
                                    elseif ($value['payment_status'] == 1) {
                                         $pay = 'Success';
                                    }
                                    else{
                                        $pay = 'Fail';
                                    }
                                    ?>

                                    <td class="center">{{$pay}}</td>

                                </tr>
                               <?php endforeach; ?>
                               @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
        <!-- /.inner -->
    </div>
    <!-- /.outer -->
@stop
@section('footer_scripts')
    <!--Plugin scripts-->
    <script type="text/javascript" src="{{asset('assets/vendors/select2/js/select2.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('assets/vendors/datatables/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.buttons.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.colVis.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.html5.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.print.min.js')}}"></script>
    <!--End of plugin scripts-->
    <!--Page level scripts-->
    <script type="text/javascript" src="{{asset('assets/js/pages/users.js')}}"></script>
    <!-- end page level scripts -->
@stop
