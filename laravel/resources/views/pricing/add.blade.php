@extends('layouts/default')


<?php 


    $visakeyvalue = [];
    $processingkeyvalue = [];
    $countryKeyVlaue =[];

?>
{{-- Page title --}}
@section('title')
    pricing
@stop
{{-- page level styles --}}
@section('header_styles')
    <!--plugin styles-->
    <link rel="stylesheet" href="{{asset('assets/vendors/intl-tel-input/css/intlTelInput.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/sweetalert/css/sweetalert2.min.css')}}" />
    <!--End of plugin styles-->
    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/sweet_alert.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/form_layouts.css')}}" />
    <!-- end of page level styles -->

    <!-- global styles-->
    <link type="text/css" rel="stylesheet" media="screen" href="{{asset('assets/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/summernote/css/summernote.css')}}"/>
    <!-- end of global styles-->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/form_elements.css')}}"/>

        <!--Plugin styles-->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/select2/css/select2.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/dataTables.bootstrap.css')}}"/>
    <!--End of plugin styles-->
    <!--Page level styles-->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tables.css')}}"/>
    <!-- end of page level styles -->




@stop
@section('content')
    <header class="head">
        <div class="main-bar">
            <div class="row no-gutters">
                <div class="col-sm-5 col-lg-6 skin_txt">
                    <h4 class="nav_top_align">
                        <i class="fa fa-pencil"></i>
                        Add pricing
                    </h4>
                </div>
                <div class="col-sm-7 col-lg-6">
                    <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                        <li class="breadcrumb-item">
                            <a href="index1">
                                <i class="fa fa-home" data-pack="default" data-tags=""></i> Dashboard
                            </a>
                        </li>
                       
                        <li class="active breadcrumb-item"> Add pricing</li>
                    </ol>
                </div>
            </div>
        </div>
    </header>
    <div class="outer">
        <div class="inner bg-container">
            <div class="row">
              
                <!-- basic sign up form-->
                <div class="col-12 col-xl-12">
                    <div class="card m-t-35">
                        <div class="card-header bg-white">
                            Add pricing
                        </div>
                        <div class="card-block">

                        <div class="form-group row m-t-35">
                            <div class="col-lg-3">
                                <div class="input-group">
                                <span class="input-group-addon">
                                <i class="fa fa-visa"></i>
                                </span>
                                    <select name="" id="countrySelector"  class="form-control">
                                        <option value="null">Select Country</option>
                                        @if(count($country))
                                            @foreach($country as $c => $coou)
                                            <?php $countryKeyVlaue[$coou['id']] =$coou['country_name']; ?>

                                                        @if(isset($priceinfo['country_id']) && $priceinfo['country_id'] == $coou['id']) 
                                                       <option value="{{$coou['id']}}" selected="">{{$coou['country_name']}}</option>
                                                        @else
                                                       <option value="{{$coou['id']}}">{{$coou['country_name']}}</option>
                                                        @endif


                                            
                                            @endforeach
                                        @endif    
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="addformRow"  <?php 
                                            echo isset($priceinfo['id'])?
                                                              'style="display: block;"':
                                                            'style="display: none;"' ?> >
                            <form class="form-horizontal" action="{{ URL::to('admin/addpricing') }}" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="country_id" id="country_id" class="form-control" placeholder="" required=""  value="{{isset($priceinfo['country_id'])?$priceinfo['country_id']:''}}" required="">

                                <input type="hidden" name="id"  class="form-control" placeholder="" required=""  value="{{isset($priceinfo['id'])?$priceinfo['id']:''}}" >

                                <fieldset>
                                    <!-- Name input-->

                                     <div class="form-group row m-t-35">



                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                <i class="fa "></i>
                                                </span>
                                               <input type="text" name="product_name"  class="form-control" placeholder="Product Name " required=""  value="{{isset($priceinfo['product_name'])?$priceinfo['product_name']:''}}" >
                                            </div>
                                        </div>



                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                <i class="fa fa-visa"></i>
                                                </span>
                                              <select name="visa_id"  class="form-control" required="required">
                                                <option value="">Select Visa Type</option>
                                                    @if(count($visatypes))
                                                    @foreach($visatypes as $c => $visa)
                                                    <?php $visakeyvalue[$visa['id']] =$visa['visa_name']; ?>

                                                        @if(isset($priceinfo['visa_id']) && $priceinfo['visa_id'] == $visa['id']) 
                                                        <option value="{{$visa['id']}}" selected="">{{$visa['visa_name']}}</option>
                                                        @else
                                                         <option value="{{$visa['id']}}" >{{$visa['visa_name']}}</option>
                                                        @endif


                                                    @endforeach
                                                    @endif    
                                            </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                            <i class="fa fa-visa"></i>
                                            </span>
                                                 <select name="processing_id"  class="form-control" required="required">
                                                    <option value="">Select Processsing Type</option>
                                                        @if(count($processing))
                                                        @foreach($processing as $p => $processs)
                                                         <?php $processingkeyvalue[$processs['id']] =$processs['processing']; ?>
                                                        

                                                        @if(isset($priceinfo['processing_id']) && $priceinfo['processing_id'] == $processs['id']) 
                                                        <option value="{{$processs['id']}}" selected="">{{$processs['processing']}}</option>
                                                        @else
                                                        <option value="{{$processs['id']}}" >{{$processs['processing']}}</option>
                                                        @endif


                                                        @endforeach
                                                        @endif    
                                                 </select>
                                            </div>
                                        </div>

                                        

                                        <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                <i class="fa fa-money"></i>
                                                </span>
                                               <input type="text" name="amount"  class="form-control" placeholder="Amount (INR)" required=""  value="{{isset($priceinfo['amount'])?$priceinfo['amount']:''}}" >
                                            </div>
                                        </div>
                                        


                                        

                                   </div>
                                           

                                     <div class="form-group row m-t-35">
                                            <div class="col-lg-3">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                <i class="fa "></i>
                                                </span>
                                              <textarea class="form-control" name="productinfo" placeholder="Product Information">{{isset($priceinfo['productinfo'])?$priceinfo['productinfo']:''}}</textarea>
                                            </div>
                                        </div>



                                        <div class="col-lg-3">
                                            <button class="btn btn-primary ">Save</button>
                                        </div>
                                    </div>

                                </fieldset>
                            </form>
                        </div>
                        </div>

                    </div>
                   
                </div>

              
                <!-- end of horizontal signin layout-->
            </div>


             

         
        </div>
        <!-- /.inner -->
    </div>



 <div class="outer">
        <div class="inner bg-container">
            <div class="card">
                <div class="card-header bg-white">
                    Pricing List
                </div>
                <div class="card-block m-t-35" id="user_body">
                    <div class="table-toolbar">
                        
                        <div class="btn-group float-right users_grid_tools">
                            <div class="tools"></div>
                        </div>
                    </div>
                    <div>
                        <div>
                            <table class="table  table-striped table-bordered table-hover dataTable no-footer"
                                   id="editable_table" role="grid">
                                <thead>
                                <tr role="row">
                                    <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">Id</th>
                                    <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">Country</th>
                                    <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">Visa </th>
                                    <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">Processing</th>
                                    <th class="sorting_asc wid-20" tabindex="0" rowspan="1" colspan="1">Amount</th>
                                    <th>Action</th>
                                 </tr>
                                </thead>
                                <tbody>    
                                            @if(count($results))
                                            @foreach($results as $p => $result)
                                            <tr role="row" class="even">
                                            <td class="sorting_1"> <?php echo isset($result['id'])?$result['id']:'';  ?></td>
                                            <td class=""><?php echo isset($countryKeyVlaue[$result['country_id']])?$countryKeyVlaue[(string)$result['country_id']]:'';  ?></td>
  

                                            <td class=""> <?php echo isset($visakeyvalue[(string)$result['visa_id']])?$visakeyvalue[(string)$result['visa_id']]:'';  ?>  </td>
                                            <td class="">{{$processingkeyvalue[(string)$result['processing_id']]}}</td>
                                            <td class="">{{$result['amount']}}</td>
                                      
                                            <td>
                                                <a href="{{ URL::to('admin/addpricing') }}?id={{$result['id']}}" data-toggle="tooltip" data-placement="top"
                                           title="View/Edit pricing"><i class="fa fa-eye text-success"></i></a>

                                           &nbsp; &nbsp;&nbsp;
                                        &nbsp;
                                                <a href="{{ URL::to('admin/deletepricing') }}?id={{$result['id']}}" data-toggle="tooltip" data-placement="top"
                                            title="
                                            delete Pricing"><i class="fa fa-trash text-danger"></i></a></td>
                                            </tr>
                                            @endforeach
                                            @endif   

                                 
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
        </div>
        <!-- /.inner -->
    </div>

@stop
@section('footer_scripts')
    <!--Plugin scripts-->
    <script type="text/javascript" src="{{asset('assets/vendors/intl-tel-input/js/intlTelInput.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/sweetalert/js/sweetalert2.min.js')}}"></script>
    <!--End of Plugin scripts-->
    <!--Page level scripts-->
    <script type="text/javascript" src="{{asset('assets/js/pages/form_layouts.js')}}"></script>
    <!-- end of page level js -->
     <!--Plugin scripts-->
 
    <script type="text/javascript" src="{{asset('assets/vendors/bootstrap3-wysihtml5-bower/js/bootstrap3-wysihtml5.all.min.js')}}"></script>
    <!--Page level scripts-->
    <script type="text/javascript" src="{{asset('assets/js/pages/form_editors.js')}}"></script>

    <!-- end page level scripts -->
 <script type="text/javascript" src="{{asset('assets/vendors/select2/js/select2.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('assets/vendors/datatables/js/dataTables.responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.buttons.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.colVis.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.html5.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.print.min.js')}}"></script>
    <!--End of plugin scripts-->
    <!--Page level scripts-->
    <script type="text/javascript" src="{{asset('assets/js/pages/users.js')}}"></script>
    <script type="text/javascript">
        
        $("#countrySelector").on('change',function(){
           if($(this).val() != 'null')
           {
            $(".addformRow").css('display','block');
            $("#country_id").val($(this).val());
           }
           else{
            $(".addformRow").css('display','none')
             $("#country_id").val('');
           }
        })
    </script>
@stop
