            <ul id="menu">
            

                     <li {!! (Request::is('setting')? 'class="active"':"") !!}>
                        <a href="{{ URL::to('admin/website') }} ">
                            <i class="fa fa-cogs"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Website</span>
                        </a>
                    </li>

                     <li {!! (Request::is('lead')? 'class="active"':"") !!}>
                        <a href="{{ URL::to('admin/payments') }} ">
                            <i class="fa fa-users"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Payments</span>
                        </a>
                    </li>

                     <li {!! (Request::is('lead')? 'class="active"':"") !!}>
                        <a href="{{ URL::to('admin/country') }} ">
                            <i class="fa fa-flag"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Country</span>
                        </a>
                    </li>

                     <li {!! (Request::is('lead')? 'class="active"':"") !!}>
                        <a href="{{ URL::to('admin/visa') }} ">
                            <i class="fa fa-cc-visa"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Visa</span>
                        </a>
                    </li>

                     <li {!! (Request::is('lead')? 'class="active"':"") !!}>
                        <a href="{{ URL::to('admin/processing') }} ">
                            <i class="fa fa-spinner"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Processing</span>
                        </a>
                    </li>

                     <li {!! (Request::is('lead')? 'class="active"':"") !!}>
                        <a href="{{ URL::to('admin/additionalprice') }} ">
                            <i class="fa fa-plus"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Additional Service</span>
                        </a>
                    </li>


                    <li {!! (Request::is('lead')? 'class="active"':"") !!}>
                        <a href="{{ URL::to('admin/pricing') }} ">
                            <i class="fa fa-money"></i>
                            <span class="link-title menu_hide">&nbsp;&nbsp;Pricing</span>
                        </a>
                    </li>


              
                </ul>