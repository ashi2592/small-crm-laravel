-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 21, 2018 at 09:28 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `payumoney`
--

-- --------------------------------------------------------

--
-- Table structure for table `additionalprice`
--

CREATE TABLE `additionalprice` (
  `id` int(11) NOT NULL,
  `additionalservice` text NOT NULL,
  `additionalkey` varchar(200) NOT NULL,
  `price` float(10,2) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `additionalprice`
--

INSERT INTO `additionalprice` (`id`, `additionalservice`, `additionalkey`, `price`, `status`, `updated_at`, `created_at`) VALUES
(1, 'Car Pick up', 'Car Pick up', 3003.00, 1, '2018-07-03 14:44:53', '2018-06-16 13:18:37'),
(2, 'Airport Fast-Track', 'fast-track', 2000.00, 1, '2018-07-03 14:44:37', '2018-07-03 09:14:37');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `country_name` varchar(200) NOT NULL,
  `code` varchar(20) NOT NULL,
  `flag_image` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `country_name`, `code`, `flag_image`, `status`, `updated_at`, `created_at`) VALUES
(3, 'Vietnamese', 'Vietnamese', NULL, 1, '2018-07-03 09:10:42', '2018-07-03 09:10:42'),
(4, 'Azezrbaijan', '3', NULL, 1, '2018-07-03 10:08:16', '2018-07-03 10:08:16');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `txnid` varchar(200) NOT NULL,
  `firstname` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `amount` int(11) NOT NULL,
  `productinfo` varchar(200) NOT NULL,
  `website_id` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `payment_date` date NOT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `txnid`, `firstname`, `email`, `phone`, `amount`, `productinfo`, `website_id`, `payment_status`, `payment_date`, `updated_at`, `created_at`) VALUES
(1, '6dd3d2cccf7ecefa6f68', 'Ashish', 'ashishemail01@gmail.com', '9090329023', 9503, 'Product Azerbaijan Normal Visa', 6, 0, '2018-03-07', '2018-07-03 15:46:48', '2018-07-03 10:16:48'),
(2, '8b0f5844911a5ef74db4', 'Ashish', 'ashishemail01@gmail.com', '9090329023', 7003, 'Product Visa for One Month', 6, 0, '2018-03-07', '2018-07-03 15:47:53', '2018-07-03 10:17:53'),
(3, 'e79833787d2bab8304f8', 'Ashish', 'ashishemail01@gmail.com', '9090329023', 9503, 'Product Azerbaijan Normal Visa', 6, 0, '2018-05-07', '2018-07-05 05:51:39', '2018-07-05 00:21:39');

-- --------------------------------------------------------

--
-- Table structure for table `pricing`
--

CREATE TABLE `pricing` (
  `id` int(11) NOT NULL,
  `visa_id` int(11) NOT NULL,
  `processing_id` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `product_name` varchar(200) DEFAULT NULL,
  `productinfo` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pricing`
--

INSERT INTO `pricing` (`id`, `visa_id`, `processing_id`, `amount`, `country_id`, `product_name`, `productinfo`, `status`, `updated_at`, `created_at`) VALUES
(1, 1, 1, 2000, 3, 'Visa for One Month', NULL, 1, '2018-07-03 14:51:45', '2018-07-03 09:21:45'),
(6, 3, 1, 1200, 3, '3 Month Payments', 'e', 1, '2018-07-03 15:16:08', '2018-07-03 09:46:08'),
(7, 6, 4, 4500, 4, 'Azerbaijan Normal Visa', NULL, 1, '2018-07-03 15:44:49', '2018-07-03 10:14:49');

-- --------------------------------------------------------

--
-- Table structure for table `processing`
--

CREATE TABLE `processing` (
  `id` int(11) NOT NULL,
  `processing` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `processing`
--

INSERT INTO `processing` (`id`, `processing`, `status`, `updated_at`, `created_at`) VALUES
(1, 'Normal', 1, '2018-07-03 14:42:47', '2018-06-16 13:18:55'),
(2, 'Urgent (1 working day)', 1, '2018-07-03 14:43:38', '2018-07-03 09:13:38'),
(3, 'Super urgent (4 working hours)', 1, '2018-07-03 14:43:57', '2018-07-03 09:13:57'),
(4, '3 working days', 1, '2018-07-03 15:42:33', '2018-07-03 10:12:33'),
(5, 'Urgent (24 Hours)', 1, '2018-07-03 15:43:02', '2018-07-03 10:13:02');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `remember_token` varchar(200) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `updated_at`, `created_at`) VALUES
(2, 'Ashish', 'ashish@gmail.com', '$2y$10$NSgLeQOd175pCMy2gmp8U.WrUzYK7lPdiiVvbvhszOCcbCJ/FfJyq', 'GP1EkGKIwIYAiF1uh94gjgjpggWE7Iy9I4zTmSy5zksrxeTBndXmHr2vRNS3', '2018-04-14 20:03:04', '2018-04-19 05:16:08'),
(3, 'sujit', 'sujitnet007@gmail.com', '$2y$10$VONR3XI2KKge1j7GzkjEFutIUULcmIwlzdTaIq70mTmM2dtVJuGXC', '46tXXQAN1u5zlnvYRCBkBmlnfA0hDkszmWtidnE8DwFalliZ2WdVM4aNlJS3', '2018-04-19 08:05:28', '2018-04-19 08:05:28');

-- --------------------------------------------------------

--
-- Table structure for table `visa`
--

CREATE TABLE `visa` (
  `id` int(11) NOT NULL,
  `visa_name` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `visa`
--

INSERT INTO `visa` (`id`, `visa_name`, `status`, `updated_at`, `created_at`) VALUES
(1, '1 Month Single Entry', 1, '2018-07-03 14:41:12', '2018-06-16 12:17:28'),
(2, '1 Month Multiple Entry', 1, '2018-07-03 14:41:48', '2018-07-03 09:11:48'),
(3, '3 Month Multiple Entry', 1, '2018-07-03 14:42:04', '2018-07-03 09:11:48'),
(4, '3 Month Single Entry', 1, '2018-07-03 14:42:29', '2018-07-03 09:12:29'),
(5, 'Normal Visa', 1, '2018-07-03 15:39:02', '2018-07-03 10:09:02'),
(6, 'Normal AZ', 1, '2018-07-03 15:40:35', '2018-07-03 10:10:35'),
(7, 'Urgent AZ', 1, '2018-07-03 15:41:38', '2018-07-03 10:11:38');

-- --------------------------------------------------------

--
-- Table structure for table `website`
--

CREATE TABLE `website` (
  `id` int(11) NOT NULL,
  `webiste_name` text NOT NULL,
  `payment_gateway_url_live` text NOT NULL,
  `payment_gateway_url_test` text NOT NULL,
  `payment_gateway_mode` int(11) NOT NULL DEFAULT '1',
  `marchent_key` varchar(30) NOT NULL,
  `salt_key` varchar(20) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `payment_gateway_surl` text NOT NULL,
  `payment_gateway_furl` text NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `website`
--

INSERT INTO `website` (`id`, `webiste_name`, `payment_gateway_url_live`, `payment_gateway_url_test`, `payment_gateway_mode`, `marchent_key`, `salt_key`, `status`, `payment_gateway_surl`, `payment_gateway_furl`, `updated_at`, `created_at`) VALUES
(6, 'http://travelasiadesk.com', 'https://secure.payu.in', 'https://sandboxsecure.payu.in', 2, 'HVxEBBqC', 'TauzDhCUZv', 1, 'http://travelasiadesk.com/payumoney/public/payments-furl', 'http://travelasiadesk.com/payumoney/public/payments-surl', '2018-07-03 14:40:21', '2018-07-03 14:40:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `additionalprice`
--
ALTER TABLE `additionalprice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pricing`
--
ALTER TABLE `pricing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `processing`
--
ALTER TABLE `processing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visa`
--
ALTER TABLE `visa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `website`
--
ALTER TABLE `website`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `additionalprice`
--
ALTER TABLE `additionalprice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pricing`
--
ALTER TABLE `pricing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `processing`
--
ALTER TABLE `processing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `visa`
--
ALTER TABLE `visa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `website`
--
ALTER TABLE `website`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
